﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Pipeline
{
    public static class ContentPaths
    {
        public static String TexturePackerExecutable
        {
            get
            {
                return System.Environment.GetEnvironmentVariable("TEXTURE_PACKER_EXECUTABLE") ??
                    @"C:\Program Files\CodeAndWeb\TexturePacker\bin\TexturePacker.exe";
            }
        }

        public static String GetContentRoot(String filename)
        {
            String dir = Path.GetDirectoryName(filename);
            do
            {
                String csproj = Path.Combine(dir, "Content.contentproj");
                if (File.Exists(csproj)) return dir;

                dir = Path.GetDirectoryName(dir);
            } while (Directory.Exists(dir));

            throw new Exception("Could not find content root above: " + filename);
        }

        public static String ChopRootForPath(String filename)
        {
           return filename.Substring(GetContentRoot(filename).Length).TrimStart('\\', '/');
        }
    }
}
