﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Pipeline
{
    public static class ContentWriterExtensions
    {
        public static void Write(this ContentWriter output, Rectangle rect)
        {
            output.Write(rect.X);
            output.Write(rect.Y);
            output.Write(rect.Width);
            output.Write(rect.Height);
        }
    }
}
