﻿using Tracksuit.Pipeline.ExcelParser;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Tracksuit.Pipeline
{
    public class I18NBundleContent
    {
        public readonly List<Tuple<String, String>> Entries;
        public I18NBundleContent(List<Tuple<String, String>> entries)
        {
            Entries = entries;
        }
    }

    [ContentImporter(".i18n-xml", DisplayName = "I18N Importer (Excel 2003) - Tracksuit")]
    public class Excel2003I18NImporter : ContentImporter<I18NBundleContent>
    {
        public override I18NBundleContent Import(string filename, ContentImporterContext context)
        {
            Excel2004XmlRowParser<Tuple<String, String>> parser = new Excel2004XmlRowParser<Tuple<string, string>>(true,
                row => Tuple.Create(row[0], row[1]));

            return new I18NBundleContent(parser.Parse(filename));
        }
    }



    [ContentTypeWriter]
    public class I18NWriter : ContentTypeWriter<I18NBundleContent>
    {
        protected override void Write(ContentWriter output, I18NBundleContent value)
        {
            output.Write(value.Entries.Count);
            foreach (var kvp in value.Entries)
            {
                output.Write(kvp.Item1);
                output.Write(kvp.Item2);
            }
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Tracksuit.Internationalization.I18NBundleReader, Tracksuit";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "Tracksuit.Internationalization.I18NBundle, Tracksuit";
        }
    }
}