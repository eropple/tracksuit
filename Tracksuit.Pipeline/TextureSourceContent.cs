﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Pipeline
{
    public class TextureSourceContent
    {
        public readonly Texture2DContent Texture;

        public TextureSourceContent(Texture2DContent texture)
        {
            Texture = texture;
        }
    }

    [ContentImporter(".png", DisplayName = "Texture Source Importer - Tracksuit", DefaultProcessor = "TextureSourceProcessor")]
    public class TextureSourceImporter : ContentImporter<String>
    {
        public override String Import(String filename, ContentImporterContext context)
        {
            return filename;
        }
    }

    [ContentProcessor(DisplayName = "Texture Source Processor - Tracksuit")]
    public class TextureSourceProcessor : ContentProcessor<String, TextureSourceContent>
    {
        public override TextureSourceContent Process(string input, ContentProcessorContext context)
        {
            var reference = new ExternalReference<TextureContent>(input);

            OpaqueDataDictionary data = new OpaqueDataDictionary();
            data.Add("PremultiplyAlpha", true);



            Texture2DContent texture = (Texture2DContent)context.BuildAndLoadAsset<TextureContent, TextureContent>(reference,
                                            "MGTextureProcessor", data,
                                            "TextureImporter");

            return new TextureSourceContent(texture);
        }
    }

    [ContentTypeWriter]
    public class TextureSourceWriter : ContentTypeWriter<TextureSourceContent>
    {
            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return "Tracksuit.Graphics.Sources.TextureSourceReader, Tracksuit";
            }

            public override string GetRuntimeType(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return "Tracksuit.Graphics.Sources.TextureSource, Tracksuit";
            }

            protected override void Write(ContentWriter output, TextureSourceContent value)
            {
                output.WriteObject((TextureContent)value.Texture);
            }
    }
}
