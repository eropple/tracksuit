﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Tracksuit.Common;

namespace Tracksuit.Pipeline.Mods
{
    public class ModInfoContent
    {
        public String UniqueName;
        public String FriendlyName;
        public String FriendlyDescription;
        public String AuthorName;
        public String AuthorContact;
        public String ScriptNamespace;
        public ModVersionContent Version;
        public List<ModConstraintContent> Constraints = new List<ModConstraintContent>();
    }

    public class ModVersionContent
    {
        public Int32 Major;
        public Int32 Minor;
        public Int32 Patch;

        public ModVersionContent(int major, int minor, int patch)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
        }

        public void Write(ContentWriter writer)
        {
            writer.Write(Major);
            writer.Write(Minor);
            writer.Write(Patch);
        }

        public static ModVersionContent Parse(String input)
        {
            try
            {
                var tokens = input.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries).Select(t => t.Trim()).Select(t => Int32.Parse(t)).ToArray();
                switch (tokens.Length)
                {
                    case 1:
                        return new ModVersionContent(tokens[0], 0, 0);
                    case 2:
                        return new ModVersionContent(tokens[0], tokens[1], 0);
                    case 3:
                        return new ModVersionContent(tokens[0], tokens[1], tokens[2]);
                    default:
                        throw new Exception("Mod version string has must have 1 to 3 tokens.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to parse input string: " + input, ex);
            }
        }
    }

    public class ModConstraintContent
    {
        public String UniqueName = "";
        public ModVersionContent MinimumVersion = null;
        public ModVersionContent MaximumVersion = null;

        public void Write(ContentWriter writer)
        {
            MinimumVersion = MinimumVersion ?? new ModVersionContent(0, 0, 0);
            MaximumVersion = MaximumVersion ?? new ModVersionContent(9999, 9999, 9999);
            writer.Write(UniqueName);
            MinimumVersion.Write(writer);
            MaximumVersion.Write(writer);
        } 
    }

    [ContentImporter(".mod-xml", DisplayName = "Mod Info - Tracksuit")]
    public class ModInfoImporter : ContentImporter<ModInfoContent>
    {
        public override ModInfoContent Import(string filename, ContentImporterContext context)
        {
            ModInfoContent info = new ModInfoContent();
            XDocument doc = XDocument.Load(filename);
            XElement root = doc.Element("Mod");

            info.UniqueName = XMLUtils.GetValueFromElement(root.Element("UniqueName"), null);
            if (info.UniqueName == null) throw new Exception("Mod must have UniqueName element.");
            info.ScriptNamespace = XMLUtils.GetValueFromElement(root.Element("ScriptNamespace"), info.UniqueName);
            info.FriendlyName = XMLUtils.GetValueFromElement(root.Element("FriendlyName"), "No Name Given");
            info.FriendlyDescription = XMLUtils.GetValueFromElement(root.Element("FriendlyDescription"),
                "No description given.", true);
            info.AuthorName = XMLUtils.GetValueFromElement(root.Element("AuthorName"), "Anonymous");
            info.AuthorContact = XMLUtils.GetValueFromElement(root.Element("AuthorContact"), "nobody@example.com");

            info.Version = ModVersionContent.Parse(XMLUtils.GetValueFromElement(root.Element("Version"), "0.1.0"));

            XElement constraintNode = root.Element("Constraints");
            info.Constraints = constraintNode == null
                ? new List<ModConstraintContent>()
                : constraintNode.Elements("Constraint")
                    .Select(node =>
                    {
                        ModConstraintContent c = new ModConstraintContent();
                        c.UniqueName = XMLUtils.GetValueFromAttribute(node, "UniqueName", null);
                        if (c.UniqueName == null) throw new Exception("All constraints must have a uniqueName.");
                        ModVersionContent min =
                            ModVersionContent.Parse(
                                XMLUtils.GetValueFromAttribute(node,
                                    "MinimumVersion", "0.0.0"));
                        ModVersionContent max =
                            ModVersionContent.Parse(
                                XMLUtils.GetValueFromAttribute(node,
                                    "MaximumVersion",
                                    "9999.9999.9999"));
                        return c;
                    }).ToList();
                
            return info;
        }
    }

    [ContentTypeWriter]
    public class ModInfoWriter : ContentTypeWriter<ModInfoContent>
    {
        protected override void Write(ContentWriter output, ModInfoContent value)
        {
            output.Write(value.UniqueName);
            output.Write(value.FriendlyName);
            output.Write(value.FriendlyDescription);
            output.Write(value.AuthorName);
            output.Write(value.AuthorContact);
            output.Write(value.ScriptNamespace);
            value.Version.Write(output);
            output.Write(value.Constraints.Count);
            foreach (var c in value.Constraints) c.Write(output);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Tracksuit.Mods.ModInfoReader, Tracksuit";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "Tracksuit.Mods.ModInfo, Tracksuit";
        }
    }
}
