﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Tracksuit.Pipeline
{
    public class AtlasSourceContent
    {
        public readonly TextureSourceContent Source;
        public readonly Dictionary<String, Rectangle> Entries;

        public AtlasSourceContent(TextureSourceContent source, Dictionary<String, Rectangle> entries)
        {
            Source = source;
            Entries = entries;
        }
    }

    [ContentImporter(".tps", DisplayName = "Atlas Importer (Texture Packer Pro) - Tracksuit", DefaultProcessor = "AtlasSourceProcessor")]
    public class TPPAtlasSourceImporter : ContentImporter<String>
    {
        public override String Import(string filename, ContentImporterContext context)
        {
            String tempDir = Path.Combine(context.IntermediateDirectory);
            String dataFile = Path.Combine(tempDir, Path.GetFileNameWithoutExtension(filename) + ".xml");
            String assetFile = Path.Combine(tempDir, Path.GetFileNameWithoutExtension(filename) + ".png");

            String commandLine = String.Format(
                "--format xml --texture-format png --max-width 4096 --max-height 4096 --opt RGBA8888 --disable-clean-transparency --disable-rotation " +
                "--algorithm MaxRects --maxrects-heuristics Best " +
                "--data \"{0}\" --sheet \"{1}\" \"{2}\"",
            dataFile, assetFile, filename);

            var ps = new ProcessStartInfo(ContentPaths.TexturePackerExecutable, commandLine);
            ps.UseShellExecute = false;
            var process = Process.Start(ps);
            process.WaitForExit();

            if (process.ExitCode != 0)
            {
                throw new Exception(String.Format("Texture Packer returned error code: {0} for line: \n\n\n{1}\n\n\n", process.ExitCode, commandLine));
            }

            return dataFile;
        }
    }

    [ContentProcessor(DisplayName = "Atlas Processor - Tracksuit")]
    public class AtlasSourceProcessor : ContentProcessor<String, AtlasSourceContent>
    {
        public override AtlasSourceContent Process(String input, ContentProcessorContext context)
        {
//<TextureAtlas imagePath="blerp.png" width="451" height="354">
//    <sprite n="adam-normal.png" x="2" y="2" w="447" h="350"/>
//</TextureAtlas>

            var entries = new Dictionary<String, Rectangle>();

            XDocument xml = XDocument.Load(input);
            foreach (XElement sprite in xml.Root.Elements("sprite"))
            {
                String name = sprite.Attribute("n").Value;
                Rectangle rect = new Rectangle(Int32.Parse(sprite.Attribute("x").Value), Int32.Parse(sprite.Attribute("y").Value),
                                               Int32.Parse(sprite.Attribute("w").Value), Int32.Parse(sprite.Attribute("h").Value));

                entries.Add(name, rect);
            }

            var ext = new ExternalReference<String>(Path.Combine(context.IntermediateDirectory, xml.Root.Attribute("imagePath").Value));
            TextureSourceContent source = context.BuildAndLoadAsset<String, TextureSourceContent>(ext, "TextureSourceProcessor", null, "TextureSourceImporter");

            return new AtlasSourceContent(source, entries);
        }
    }

    [ContentTypeWriter]
    public class AtlasSourceWriter : ContentTypeWriter<AtlasSourceContent>
    {
        protected override void Write(ContentWriter output, AtlasSourceContent value)
        {
            output.WriteObject(value.Source);
            output.Write(value.Entries.Count);
            foreach (String key in value.Entries.Keys)
            {
                output.Write(key);
                output.Write(value.Entries[key]);
            }
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Tracksuit.Graphics.Sources.AtlasSourceReader, Tracksuit";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "Tracksuit.Graphics.Sources.AtlasSource, Tracksuit";
        }
    }
}