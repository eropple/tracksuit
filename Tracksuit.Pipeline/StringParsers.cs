﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Pipeline
{
    public static class StringParsers
    {
        public static String FromVector2(Vector2 input)
        {
            return String.Format("{0}, {1}", input.X, input.Y);
        }
        public static Vector2 ToVector2(String input)
        {
            float[] components = input.Split(',').Select(t => Single.Parse(t.Trim())).ToArray();

            if (components.Length != 2) throw new Exception(String.Format("Invalid vector2 input '{0}', needs 2 components.", input));

            return new Vector2(components[0], components[1]);
        }

        public static String FromColor(Color color)
        {
            return String.Format("{0}, {1}, {2}, {3}", (255.0f / color.R), (255.0f / color.G), (255.0f / color.B), (255.0f / color.A));
        }
        public static Color ToColor(String input)
        {
            float[] components = input.Split(',').Select(t => Single.Parse(t.Trim())).ToArray();

            if (components.Length == 3)
            {
                return new Color(components[0], components[1], components[2], 1.0f);
            }
            else if (components.Length == 4)
            {
                return new Color(components[0], components[1], components[2], components[3]);
            }
            else
            {
                throw new Exception(String.Format("Invalid  color input '{0}', needs 3 or 4 components.", input));
            }
        }

        public static String FromBoolean(Boolean boolean)
        {
            return boolean ? "yes" : "no";
        }
        public static Boolean ToBoolean(String input)
        {
            input = input.Trim();
            return String.Equals(input, "yes", StringComparison.OrdinalIgnoreCase) ||
                   String.Equals(input, "true", StringComparison.OrdinalIgnoreCase) ||
                   String.Equals(input, "1", StringComparison.OrdinalIgnoreCase);
        }
    }
}
