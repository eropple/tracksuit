﻿using Tracksuit.Pipeline.BMFontHelpers;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Pipeline
{
    public class BMFontContent
    {
        public readonly FontFile File;
        public readonly Dictionary<Int32, TextureSourceContent> Sources = new Dictionary<int, TextureSourceContent>();
        public BMFontContent(FontFile file)
        {
            File = file;
        }
    }

    [ContentImporter(".font-xml", DisplayName = "Bitmap Font Importer - Tracksuit", DefaultProcessor = "BMFontProcessor")]
    public class BMFontImporter : ContentImporter<String>
    {
        public override String Import(string filename, ContentImporterContext context)
        {
            return filename;
        }
    }

    [ContentProcessor(DisplayName = "Bitmap Font Processor - Tracksuit")]
    public class BMFontProcessor : ContentProcessor<String, BMFontContent>
    {
        public override BMFontContent Process(String input, ContentProcessorContext context)
        {
            try
            {
                FontFile file = FontLoader.Load(input);

                BMFontContent content = new BMFontContent(file);
                foreach (FontPage page in file.Pages)
                {
                    var ext = new ExternalReference<String>(Path.Combine(Path.GetDirectoryName(input), page.File));
                    TextureSourceContent source = context.BuildAndLoadAsset<String, TextureSourceContent>(ext, "TextureSourceProcessor", null, "TextureSourceImporter");

                    content.Sources.Add(page.ID, source);
                }

                return content;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }
        }
    }

    [ContentTypeWriter]
    public class BMFontWriter : ContentTypeWriter<BMFontContent>
    {
        protected override void Write(ContentWriter output, BMFontContent value)
        {
            output.Write(value.File.Info.Face);
            output.Write(value.File.Common.LineHeight);
            output.Write(value.File.Common.Base);

            output.Write(value.Sources.Count);

            foreach (var kvp in value.Sources)
            {
                output.Write(kvp.Key);
                output.WriteObject(kvp.Value);
            }

            output.Write(value.File.Chars.Count);
            foreach (var c in value.File.Chars)
            {
                output.Write(c.ID);
                output.Write(c.X);
                output.Write(c.Y);
                output.Write(c.Width);
                output.Write(c.Height);
                output.Write(c.XOffset);
                output.Write(c.YOffset);

                output.Write(c.XAdvance);
                output.Write(c.Page);
            }

            output.Write(value.File.Kernings.Count);
            foreach (var k in value.File.Kernings)
            {
                output.Write(k.First);
                output.Write(k.Second);
                output.Write(k.Amount);
            }
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Tracksuit.Graphics.Fonts.BitmapFontReader, Tracksuit";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "Tracksuit.Graphics.Fonts.BitmapFont, Tracksuit";
        }
    }
}
