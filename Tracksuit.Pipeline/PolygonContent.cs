﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Pipeline
{
    public class PolygonContent
    {
        public List<Vector2> Vertices = new List<Vector2>();
    }

    [ContentTypeWriter]
    public class PolygonWriter : ContentTypeWriter<PolygonContent>
    {
        public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
        {
            return "Tracksuit.Primitives.PolygonReader, Tracksuit";
        }

        public override string GetRuntimeType(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
        {
            return "Tracksuit.Primitives.Polygon, Tracksuit";
        }

        protected override void Write(ContentWriter output, PolygonContent value)
        {
            if (value.Vertices.Count < 3) throw new Exception("Polygons must have at least 3 vertices.");
            output.Write(value.Vertices.Count);
            foreach (Vector2 v in value.Vertices) output.Write(v);
        }
    }
}
