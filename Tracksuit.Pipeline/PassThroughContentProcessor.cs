﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace Tracksuit.Pipeline
{
    /// <summary>
    /// MonoGame loses its shit if there isn't a content processor available.
    /// </summary>
    /// <typeparam name="TContentType"></typeparam>
    public class PassThroughContentProcessor<TContentType> : ContentProcessor<TContentType, TContentType>
    {
        public override TContentType Process(TContentType input, ContentProcessorContext context)
        {
            return input;
        }
    }
}
