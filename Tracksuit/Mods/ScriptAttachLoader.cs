﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace Tracksuit.Mods
{
    public class ScriptAttachLoader<TScriptAttachType, TScriptAttachAttribute>
        where TScriptAttachType : class
        where TScriptAttachAttribute : ScriptAttachAttribute
    {
        private static readonly Object[] EmptyParams = {};

        private readonly ILog Logger = LogManager.GetCurrentClassLogger();
        private readonly Type[] _constructorTypes;
        private readonly bool _throwOnMissing;
        private Dictionary<String, Type> _cache = null;

        public ScriptAttachLoader(Type[] constructorTypes, Boolean throwOnMissing = true)
        {
            _constructorTypes = constructorTypes;
            _throwOnMissing = throwOnMissing;
        }

        public TScriptAttachType Load(String attachPoint, Object[] args = null)
        {
            args = args ?? EmptyParams;
            if (_cache == null) _cache = BuildCache();
            Type ty;
            if (!_cache.TryGetValue(attachPoint, out ty))
            {
                if (!_throwOnMissing) return null;
                throw new TracksuitException("Failed to load object for attach point '{0}'.", attachPoint);
            }
#if DEBUG
            var errors = new List<String>();
            if (args.Length != _constructorTypes.Length)
            {
                errors.Add(String.Format("Argument list has {0} items; constructor expects {1}.", args.Length,
                    _constructorTypes.Length));
            }
            else
            {
                for (Int32 i = 0; i < args.Length; ++i)
                {
                    if (!_constructorTypes[i].IsInstanceOfType(args[i]))
                    {
                        errors.Add(String.Format("Argument {0} does not match type. (Expected: {1}, actual: {2})", i,
                            _constructorTypes[i].FullName, args[i].GetType().FullName));
                    }
                }
            }
            if (errors.Count != 0)
            {
                Logger.FatalFormat("Errors instantiating object of class {0}:", typeof(TScriptAttachType).FullName);
                foreach (var s in errors) Logger.FatalFormat(" - {0}", s);
                throw new TracksuitException(
                    "Failed to load object of type '{0}' to attach point '{1}' because errors occurred (check the log).",
                    ty.FullName, attachPoint);
            }
#endif
            return ty.GetConstructor(_constructorTypes).Invoke(args) as TScriptAttachType;
        }

        protected virtual Dictionary<String, Type> BuildCache()
        {
            Logger.Debug(
                m =>
                    m("Building cache of '{0}' objects tagged with the '{1}' attribute.",
                        typeof (TScriptAttachType).FullName, typeof (TScriptAttachAttribute).FullName));
            Dictionary<String, Type> cache = new Dictionary<String, Type>(200);

            var attrType = typeof(TScriptAttachAttribute);
            foreach (Assembly a in ModSystem.ModAssemblies)
            {
                foreach (Type t in a.GetTypes())
                {
                    
                    var attr =
                        Attribute.GetCustomAttribute(t, attrType) as TScriptAttachAttribute;

                    if (attr == null) continue;

                    if (t.GetConstructor(_constructorTypes) == null)
                    {
                        var err = String.Format("Attachment class '{0}' doesn't have a public constructor that takes ({1}).",
                            t.FullName, String.Join(", ", _constructorTypes.Select(type => type.FullName)));
                        throw new ModException(err);
                    }

                    cache.Add(attr.AttachPoint, t);
                }
            }

            Logger.Debug(m => m("{0} classes found with attribute '{1}'.", cache.Count, typeof(TScriptAttachAttribute).FullName));
            return cache;
        }
    }
}
