﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using FarseerPhysics.Common;
using Microsoft.CSharp;
using Microsoft.Xna.Framework.Content;
using Tracksuit.Config;
using Path = System.IO.Path;

namespace Tracksuit.Mods
{
#if DESKTOP
    public static class ModSystem
    {
        private static readonly ILog Logger = LogManager.GetCurrentClassLogger();

        public static ReadOnlyCollection<ModInfo> ActiveMods { get; private set; }
        public static ReadOnlyCollection<Assembly> ModAssemblies { get; private set; } 
        private static String _contentDirectoryPath;
        private static String[] _compileAssemblyLocations;

        public static void Initialize(String contentDirectory)
        {
            if (ActiveMods != null) throw new ModException("Cannot double-initialize mod system.");

            // "Base" is framework-required stuff. Your game goes in "Core".
            String[] modNames = Configuration.Instance.GetString("mods.active", "Core, Base")
                                                      .Split(new [] {','}, StringSplitOptions.RemoveEmptyEntries)
                                                      .Select(t => t.Trim()).ToArray();

            Logger.Info(m => m("Available mods: {0}", String.Join("; ", modNames)));

            _contentDirectoryPath = contentDirectory;
            ContentManager tempManager = new ContentManager(Tracksuit.Game.Instance.Services, _contentDirectoryPath);
            ModInfo[] activeMods = new ModInfo[modNames.Length];

            for (Int32 i = 0; i < modNames.Length; ++i)
            {
                String path = modNames[i] + "/info.mod-xml";
                activeMods[i] = tempManager.Load<ModInfo>(path);
            }

            EnsureDependencies(activeMods);
            ActiveMods = new ReadOnlyCollection<ModInfo>(activeMods);

            var whitelistedAssemblies =
                (Configuration.Instance.GetString("scripting.assembly_whitelist",
                    Path.GetFileName(Assembly.GetEntryAssembly().Location)) + ";Tracksuit.dll;System.dll;System.Core.dll;System.Xml.Linq.dll;MonoGame.Framework.dll;FarseerPhysics MonoGame.dll;Common.Logging.dll;Common.Logging.Core.dll")
                    .Split(new[] {";"}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(a => a.Trim());
            var appLocation = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            _compileAssemblyLocations = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => !a.IsDynamic)
                .Select(a => a.Location)
                .Concat(Directory.EnumerateFiles(appLocation, "*.dll")).Distinct()
                .Where(a => whitelistedAssemblies.Contains(Path.GetFileName(a))).ToArray();

            if (Logger.IsDebugEnabled)
            {
                foreach (String a in _compileAssemblyLocations) Logger.DebugFormat("ModSystem includes assembly: {0}", Path.GetFileName(a));
            }
        }

        public static void LoadCode()
        {
            Dictionary<String, Assembly> modAssemblies = new Dictionary<String, Assembly>(ActiveMods.Count);

            CSharpCodeProvider compiler = new CSharpCodeProvider();
            foreach (ModInfo mod in ActiveMods)
            {
                BuildModAndDependencies(mod, modAssemblies, compiler, 0);
            }

            ModAssemblies = new ReadOnlyCollection<Assembly>(ActiveMods.Select(m => modAssemblies[m.UniqueName]).ToArray());
        }


        private static void BuildModAndDependencies(ModInfo mod, Dictionary<String, Assembly> modAssemblies, CSharpCodeProvider compiler, Int32 depth)
        {
            if (depth > 20) throw new ModException("Dependency cycle detected. Fix your mods + dependencies.");

            if (modAssemblies.ContainsKey(mod.UniqueName)) return;
            foreach (ModConstraint constraint in mod.Constraints)
            {
                var depMod = ActiveMods.FirstOrDefault(m => m.UniqueName == constraint.UniqueName);
                BuildModAndDependencies(depMod, modAssemblies, compiler, depth + 1);
            }
            if (modAssemblies.ContainsKey(mod.UniqueName)) return;

            Logger.Info(m => m("Loading mod '{0}'...", mod.UniqueName));

            String cachePath = Configuration.Instance.GetString("cache.path",
                Path.Combine(DesktopConfiguration.SavedConfigPath, "script_cache"));
            Directory.CreateDirectory(cachePath);

            Boolean mustRecompile = false;
            String compiledAssembly = Path.Combine(cachePath, mod.UniqueName + ".mod.dll");
            String codePath = Path.Combine(_contentDirectoryPath, mod.UniqueName, "Scripts");

            var sourceFiles = Directory.EnumerateFiles(codePath, "*.cs", SearchOption.AllDirectories).ToArray();
            var dependentAssemblies =
                mod.Constraints.Select(c => Path.Combine(cachePath, c.UniqueName + ".mod.dll")).ToArray();

            if (!File.Exists(compiledAssembly))
            {
                Logger.Debug(m => m("Assembly doesn't exist - must recompile."));
                mustRecompile = true;
            }
            else
            {
                DateTime assemblyBuildTime = File.GetLastWriteTimeUtc(compiledAssembly);
                Boolean dependencyRecompile = dependentAssemblies.Any(f => assemblyBuildTime < File.GetLastWriteTimeUtc(f));
                Logger.Debug(m => m("A dependency is newer than the last build; must recompile."));
                Boolean sourceRecompile = sourceFiles.Any(f => assemblyBuildTime < File.GetLastWriteTimeUtc(f));
                Logger.Debug(m => m("Sources have been updated since the last build; must recompile."));

                mustRecompile = mustRecompile || sourceRecompile || dependencyRecompile;

            }

            if (mustRecompile)
            {
                if (File.Exists(compiledAssembly)) File.Delete(compiledAssembly);

                if (Logger.IsDebugEnabled)
                {
                    Logger.DebugFormat("Files to compile:");
                    foreach (String sourceFile in sourceFiles) Logger.DebugFormat(" - {0}", sourceFile);
                }

                var parameters = new CompilerParameters {OutputAssembly = compiledAssembly, GenerateInMemory = true};
                parameters.ReferencedAssemblies.AddRange(_compileAssemblyLocations);
                parameters.ReferencedAssemblies.AddRange(dependentAssemblies);

                var results = compiler.CompileAssemblyFromFile(parameters, sourceFiles);
                if (results.Errors.Count > 0)
                {
                    if (Logger.IsFatalEnabled)
                    {
                        Logger.Fatal(m => m("COMPILATION FAILURE IN '{0}' - {1} ERRORS:", mod.UniqueName, results.Errors.Count));
                        foreach (var error in results.Errors) Logger.FatalFormat(error.ToString());
                    }
                    String str = "";
                    foreach (var error in results.Errors) str += error + "\n";
                    throw new ModException("Compilation failure in '{0}':\n\n{1}", compiledAssembly, str);
                }

                modAssemblies.Add(mod.UniqueName, results.CompiledAssembly);
            }
            else
            {
                Logger.Debug(m => m("No recompilation needed, adding existing assembly from cache."));
                modAssemblies.Add(mod.UniqueName, Assembly.LoadFile(compiledAssembly));
            }

            Assembly a = modAssemblies[mod.UniqueName];
        }

        private static void EnsureDependencies(IList<ModInfo> mods)
        {
            foreach (ModInfo mod in mods)
            {
                foreach (ModConstraint constraint in mod.Constraints)
                {
                    var where = mods.Where(m => m.UniqueName == constraint.UniqueName).ToArray();
                    if (where.Length != 1)
                    {
                        throw new ModException("Could not find mod '{0}', required by '{1}'.", constraint.UniqueName,
                            mod.UniqueName);
                    }
                    else
                    {
                        var version = where[0].Version;
                        var minCompare = ModVersion.Compare(constraint.MinimumVersion, version);
                        var maxCompare = ModVersion.Compare(version, constraint.MaximumVersion); 
                        if (minCompare >= 1 && maxCompare <= -1)
                        {
                            throw new ModException("'{0}' requires a version of '{1}' between {2} and {3} (got {4}).", mod.UniqueName, constraint.UniqueName, constraint.MinimumVersion, constraint.MaximumVersion, version);
                        }
                    }
                }
            }
        }
    }
#else
    #error TODO: Fake mod support for non-desktops.
#endif
}
