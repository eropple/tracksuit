﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Mods
{
    public class ModException : TracksuitException
    {
        public ModException(string message, Exception inner = null) : base(message, inner)
        {
        }

        public ModException(string message, params object[] formatObjects) : base(message, formatObjects)
        {
        }

        public ModException(Exception inner, string message, params object[] formatObjects) : base(inner, message, formatObjects)
        {
        }
    }
}
