﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Tracksuit.Content;

namespace Tracksuit.Mods
{
    public class ModInfo
    {
        public readonly String UniqueName;
        public readonly String FriendlyName;
        public readonly String FriendlyDescription;
        public readonly String AuthorName;
        public readonly String AuthorContact;
        public readonly String ScriptNamespace;
        public readonly ModVersion Version;
        public readonly ReadOnlyCollection<ModConstraint> Constraints;

        public ModInfo(string uniqueName, string friendlyName, string friendlyDescription, string authorName,
            String authorContact, String scriptNamespace, ModVersion version, IList<ModConstraint> constraints)
        {
            UniqueName = uniqueName;
            FriendlyName = friendlyName;
            FriendlyDescription = friendlyDescription;
            AuthorName = authorName;
            AuthorContact = authorContact;
            ScriptNamespace = scriptNamespace;
            Version = version;
            Constraints = new ReadOnlyCollection<ModConstraint>(constraints);
        }
    }


    public class ModVersion : IComparable<ModVersion>, IEquatable<ModVersion>
    {
        public readonly Int32 Major;
        public readonly Int32 Minor;
        public readonly Int32 Patch;

        public ModVersion(int major, int minor, int patch)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
        }
        public int CompareTo(ModVersion other)
        {
            Int32 ret = 0;
            ret = Major.CompareTo(other.Major);
            if (ret != 0) return ret;
            ret = Minor.CompareTo(other.Minor);
            if (ret != 0) return ret;
            ret = Patch.CompareTo(other.Patch);
            return ret;
        }

        public bool Equals(ModVersion other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Major == other.Major && Minor == other.Minor && Patch == other.Patch;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ModVersion) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Major;
                hashCode = (hashCode*397) ^ Minor;
                hashCode = (hashCode*397) ^ Patch;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}.{1}.{2}", Major, Minor, Patch);
        }

        public static int Compare(ModVersion left, ModVersion right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return 0;
            }
            if (object.ReferenceEquals(left, null))
            {
                return -1;
            }
            return left.CompareTo(right);
        }
    }

    public class ModConstraint
    {
        public readonly String UniqueName;
        public readonly ModVersion MinimumVersion;
        public readonly ModVersion MaximumVersion;

        public ModConstraint(String uniqueName, ModVersion minimumVersion, ModVersion maximumVersion)
        {
            UniqueName = uniqueName;
            MinimumVersion = minimumVersion;
            MaximumVersion = maximumVersion;
        }
    }

    public class ModInfoReader : BaseContentTypeReader<ModInfo>
    {
        protected override object Read(ContentReader input, object existingInstance)
        {
            String uniqueName = input.ReadString();
            String friendlyName = input.ReadString();
            String friendlyDescription = input.ReadString();
            String authorName = input.ReadString();
            String authorDescription = input.ReadString();
            String scriptNamespace = input.ReadString();
            ModVersion version = new ModVersion(input.ReadInt32(), input.ReadInt32(), input.ReadInt32());

            Int32 constraintCount = input.ReadInt32();
            List<ModConstraint> constraints = new List<ModConstraint>(constraintCount);
            for (Int32 i = 0; i < constraintCount; ++i)
            {
                String modUniqueName = input.ReadString();
                ModVersion minVersion = new ModVersion(input.ReadInt32(), input.ReadInt32(), input.ReadInt32());
                ModVersion maxVersion = new ModVersion(input.ReadInt32(), input.ReadInt32(), input.ReadInt32());

                constraints.Add(new ModConstraint(modUniqueName, minVersion, maxVersion));
            }

            return new ModInfo(uniqueName, friendlyName, friendlyDescription, authorName, authorDescription, scriptNamespace, version, constraints);
        }
    }
}
