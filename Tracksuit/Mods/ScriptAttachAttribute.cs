﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Mods
{
    [AttributeUsage(AttributeTargets.Class)]
    public abstract class ScriptAttachAttribute : Attribute
    {
        public readonly String AttachPoint;

        protected ScriptAttachAttribute(string attachPoint)
        {
            AttachPoint = attachPoint;
        }
    }
}
