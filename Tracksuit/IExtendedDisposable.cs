﻿using System;

namespace Tracksuit
{
    public interface IExtendedDisposable : IDisposable
    {
        Boolean IsDisposed { get; }
    }
}

