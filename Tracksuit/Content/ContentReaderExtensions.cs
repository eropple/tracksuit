﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Content
{
    public static class ContentReaderExtensions
    {
        public static Rectangle ReadRectangle(this ContentReader content)
        {
            return new Rectangle(content.ReadInt32(), content.ReadInt32(), content.ReadInt32(), content.ReadInt32());
        }
    }
}
