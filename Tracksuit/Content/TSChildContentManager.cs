﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;

namespace Tracksuit.Content
{
    public class TSChildContentManager : ContentManager, ITSContentManagerProvider
    {
        private readonly TSContentManager _canonicalTSParent;

        public TSChildContentManager(TSContentManager parent, String modName)
            : base(parent.ServiceProvider, Path.Combine(parent.RootDirectory, modName))
        {
            _canonicalTSParent = parent;
        }

#if DESKTOP
        public Boolean Exists(String assetName)
        {
            return File.Exists(Path.Combine(RootDirectory, assetName + ".xnb"));
        }
#else
#error Implement asset location.
#endif

        public TSContentManager CanonicalTSContentManager { get { return _canonicalTSParent; } }
    }
}
