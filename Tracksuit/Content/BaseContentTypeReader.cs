﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Content
{
    public abstract class BaseContentTypeReader<TContentType> : ContentTypeReader<TContentType>
    {
        public sealed override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public sealed override int GetHashCode()
        {
            return base.GetHashCode();
        }

        protected sealed override void Initialize(ContentTypeReaderManager manager)
        {
            base.Initialize(manager);
        }

        public sealed override string ToString()
        {
            return base.ToString();
        }

        protected sealed override TContentType Read(ContentReader input, TContentType existingInstance)
        {
            return (TContentType)Read(input, (object)existingInstance);
        }
    }
}
