﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Common.Logging;
using System.Collections.Generic;
using Tracksuit.Config;
using Tracksuit.Mods;

namespace Tracksuit.Content
{
    public class TSContentManager : ContentManager, ITSContentManagerProvider
    {
        public readonly Int32 ID;
        private readonly TSContentManager _parent;

        private readonly Dictionary<Type, Dictionary<String, Object>> _contentDict;

        private readonly TSChildContentManager[] _internalContentManagers;  

        public TSContentManager(IServiceProvider serviceProvider, string rootDirectory)
            : base(serviceProvider, rootDirectory)
        {
            ID = GetIDAndIncrement();
            _parent = null;
            _contentDict = new Dictionary<Type, Dictionary<String, Object>>();
            _internalContentManagers =
                ModSystem.ActiveMods.Select(m => new TSChildContentManager(this, m.UniqueName)).ToArray();

            Logger.Debug(m => m("Created new TSContentManager, ID #{0}.", ID));
        }

        public TSContentManager(TSContentManager parent)
            : this(parent.ServiceProvider, parent.RootDirectory)
        {
            _parent = parent;
            Logger.Debug(m => m("New TSContentManager with ID #{0} is a child of CanonicalTSContentManager ID #{1}", ID, _parent.ID));
        }

        public TSContentManager CanonicalTSContentManager { get { return this; } }

        public override TAssetType Load<TAssetType>(string assetName)
        {
            TAssetType asset;
            if (TryGetLoadedAsset(assetName, out asset, true))
                return asset;
            
#if DESKTOP
            Boolean loaded = false;
            for (Int32 i = 0; i < _internalContentManagers.Length; ++i)
            {
                if (_internalContentManagers[i].Exists(assetName))
                {
                    asset = _internalContentManagers[i].Load<TAssetType>(assetName);
                    Logger.Debug(m => m("Loaded '{0}' from mod '{1}'.", assetName, ModSystem.ActiveMods[i].UniqueName));
                    loaded = true;
                    break;
                }
            }
            if (!loaded) asset = base.Load<TAssetType>(assetName);
#else
#error TODO: implement asset loading
#endif

            Dictionary<String, Object> internalDict;
            if (!_contentDict.TryGetValue(typeof(TAssetType), out internalDict))
            {
                internalDict = new Dictionary<string, object>();
                _contentDict.Add(typeof(TAssetType), internalDict);
            }
            internalDict.Add(assetName, asset);

            return asset;
        }

        public virtual TAssetType[] LoadAll<TAssetType>(String assetName)
        {
            return
                _internalContentManagers.Where(c => c.Exists(assetName))
                    .Select(c => c.Load<TAssetType>(assetName))
                    .ToArray();
        }

        public Boolean Exists<TAssetType>(String assetName)
        {
            TAssetType asset;
            if (TryGetLoadedAsset(assetName, out asset, true)) return true;

            return _internalContentManagers.Any(c => c.Exists(assetName));
        }

        public Boolean TryGetLoadedAsset<TAssetType>(String key, out TAssetType retval, Boolean recursive = false)
        {
            Dictionary<String, Object> internalDict;

            Object asset;
            if (_contentDict.TryGetValue(typeof(TAssetType), out internalDict))
            {
                if (internalDict.TryGetValue(key, out asset))
                {
                    if (asset is TAssetType)
                    {
                        retval = (TAssetType) asset;
                        return true;
                    }

                    retval = default(TAssetType);
                    return false;
                }
            }
            else if (recursive && _parent != null)
            {
                TAssetType a2;
                if (_parent.TryGetLoadedAsset(key, out a2, true))
                {
                    retval = a2;
                    return true;
                }
                retval = default(TAssetType);
                return false;
            }

            retval = default(TAssetType);
            return false;
        }



        private static readonly ILog Logger = LogManager.GetLogger<TSContentManager>();

        private static readonly Object _managerIdLock = new Object();
        private static Int32 _managerId = 0;
        public static TSContentManager Global;

        private static Int32 GetIDAndIncrement()
        {
            lock (_managerIdLock)
            {
                return _managerId++;
            }
        }

        public static void Initialize()
        {
            var contentDirectory = Configuration.Instance.GetString("content.directory", "Content");
            ModSystem.Initialize(contentDirectory);
            Global = new TSContentManager(Game.Instance.Services, contentDirectory);
        }
    }
}

