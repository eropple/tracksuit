﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Tracksuit
{
    public static class Extensions
    {
        public static Vector3 ToVector3(this Vector2 v)
        {
            return new Vector3(v.X, v.Y, 0);
        }

        public static Color Lighten(this Color c, float amount = 0.2f)
        {
            return Color.Lerp(c, Color.White, amount);
        }
        public static Color Darken(this Color c, float amount = 0.2f)
        {
            return Color.Lerp(c, Color.Black, amount);
        }

        public static Rectangle Translate(this Rectangle r, Vector2 v)
        {
            return new Rectangle((int)(r.X + v.X), (int)(r.Y + v.Y), r.Width, r.Height);
        }

        public static Vector2 ToVector2(this Point p) {  return new Vector2(p.X, p.Y); }
    }
}
