﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace Tracksuit.Tweening
{
    public class Tweener
    {
        protected static readonly ILog Logger = LogManager.GetCurrentClassLogger();

        public Int64 CurrentTime { get; protected set; }
        public Boolean IsComplete { get; protected set; }

        public Single CurrentValue { get; protected set; }

        public TweenController Controller { get; internal set; }

        /// <summary>
        /// Value from which the tweener will start.
        /// </summary>
        public readonly Single StartValue;
        /// <summary>
        /// Value to which the tweener will 
        /// </summary>
        public readonly Single EndValue;
        /// <summary>
        /// The duration, in milliseconds, of this tweener's cycle.
        /// </summary>
        public readonly Int64 Duration;
        /// <summary>
        /// Returns to start once completed and continues. Fires OnComplete event upon reaching EndValue every time.
        /// </summary>
        public readonly Boolean Looping;

        /// <summary>
        /// The function used to compute values within the tweener.
        /// </summary>
        public readonly TweenerFunction Function;

        public event OnTweenerChangeDelegate OnChange;
        public event OnTweenerCompleteDelegate OnComplete;

        public Tweener(TweenerFunction function, float startValue, float endValue, long duration, bool looping,
            OnTweenerChangeDelegate onChange = null, OnTweenerCompleteDelegate onComplete = null)
        {
            Function = function;
            StartValue = startValue;
            EndValue = endValue;
            Duration = duration;
            Looping = looping;
            OnChange = onChange;
            OnComplete = onComplete;
        }

        public virtual void Update(Int64 delta)
        {
            if (IsComplete) return;

            CurrentTime += delta;
            CurrentValue = (Single)Function(CurrentTime, StartValue, EndValue, Duration);
//            Logger.Debug(m => m("Tweener: delta {0}, currentTime {1}, currentValue {2}", delta, CurrentTime, CurrentValue));

            RunOnChange();

            if (CurrentTime < Duration) return;
            if (Looping)
            {
                CurrentTime = (CurrentTime % Duration);
                CurrentValue = (Single) Function(CurrentTime, StartValue, EndValue, Duration);
            }
            else
            {
                CurrentTime = Duration;
                CurrentValue = EndValue;
                IsComplete = true;
                RunOnChange();
                RunOnComplete();
            }
        }

        protected void RunOnChange()
        {
            if (OnChange != null) OnChange(this, CurrentValue);
        }

        protected void RunOnComplete()
        {
            if (OnComplete != null) OnComplete(this, CurrentValue);
        }

        public void Reset()
        {
            CurrentTime = 0;
            IsComplete = false;
        }
    }

    public class BouncyTweener : Tweener
    {
        public BouncyTweener(TweenerFunction function, float startValue, float endValue, long duration, bool looping,
            OnTweenerChangeDelegate onChange = null, OnTweenerCompleteDelegate onComplete = null)
            : base(function, startValue, endValue, duration, looping, onChange, onComplete)
        {
        }

        public override void Update(Int64 delta)
        {
            if (IsComplete) return;

            CurrentTime += delta;
//            Logger.Debug(m => m("Tweener: delta {0}, currentTime {1}, currentValue {2}", delta, CurrentTime, CurrentValue));
            if (0 <= CurrentTime && CurrentTime <= Duration)
            {
                CurrentValue = (Single)Function(CurrentTime, StartValue, EndValue, Duration);
                RunOnChange();
            }
            else if (Duration < CurrentTime && CurrentTime <= Duration*2)
            {
                CurrentValue = (Single)Function(Duration - (CurrentTime - Duration), StartValue, EndValue, Duration);
                RunOnChange();
            }
            else if (CurrentTime > Duration * 2)
            {
                if (Looping)
                {
                    CurrentTime = CurrentTime%(Duration*2);
                    CurrentValue = (Single) Function(CurrentTime, StartValue, EndValue, Duration);
                    RunOnChange();
                }
                else
                {
                    CurrentTime = Duration*2;
                    CurrentValue = StartValue;
                    RunOnChange();
                    RunOnComplete();
                    IsComplete = true;
                }
            }

        }
    }

    public delegate Double TweenerFunction(double currentTime, double startValue, double endValue, double duration);

    public delegate void OnTweenerChangeDelegate(Tweener tweener, float value);

    public delegate void OnTweenerCompleteDelegate(Tweener tweener, float value);
}
