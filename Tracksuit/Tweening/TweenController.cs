﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Tweening
{
    public class TweenController
    {
        private readonly HashSet<Tweener> _tweeners = new HashSet<Tweener>();
        private readonly List<Tweener> _subTweenerList = new List<Tweener>(); 
        private readonly List<Tweener> _killList = new List<Tweener>(); 

        public void Update(Int64 delta)
        {
            foreach (var t in _subTweenerList)
            {
                _tweeners.Add(t);
                t.Controller = this;
            }
            foreach (var t in _killList)
            {
                _tweeners.Remove(t);
                t.Controller = null;
            }
            _subTweenerList.Clear();
            _killList.Clear();

            foreach (var t in _tweeners) t.Update(delta);
        }

        public void Add(Tweener item)
        {
            _subTweenerList.Add(item);
        }
        public void Remove(Tweener item)
        {
            _killList.Add(item);
        }
    }
}
