﻿using System;

namespace Tracksuit.Input
{
    public enum MouseButton
    {
        Left,
        Middle,
        Right,
        XButton1,
        XButton2
    }
}

