﻿using System;

namespace Tracksuit.Input
{
    public enum Thumbstick
    {
        Left,
        Right
    }
}