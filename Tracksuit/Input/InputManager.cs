﻿using System;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Common.Logging;

namespace Tracksuit.Input
{
    public class InputManager
    {
        private readonly ILog Logger = LogManager.GetCurrentClassLogger();

        private KeyboardState _lastKeyboard;
        private HashSet<Keys> _lastKeyboardKeys;

        public KeyboardState Keyboard { get; private set; }
        public HashSet<Keys> KeyboardKeys { get; private set; }

        private MouseState _lastMouse;
        public MouseState Mouse { get; private set; }



        private readonly Boolean[] _watchedControllers = new Boolean[4];
        private readonly GamePadState[] _lastControllerStates = new GamePadState[4];
        public readonly GamePadState[] ControllerStates = new GamePadState[4];

        public InputManager()
        {
            _lastKeyboard = Microsoft.Xna.Framework.Input.Keyboard.GetState();
            _lastKeyboardKeys = new HashSet<Keys>(_lastKeyboard.GetPressedKeys());
            Keyboard = _lastKeyboard;
            KeyboardKeys = _lastKeyboardKeys;

            _lastMouse = Microsoft.Xna.Framework.Input.Mouse.GetState();

            for (Int32 i = 0; i < 4; ++i)
            {
                _watchedControllers[i] = true;
                _lastControllerStates[i] = GamePad.GetState(ArrayIndexToPlayerIndex(i));
            }
        }

        public void Update()
        {
            // TODO: walk the various states, dispatch updates to StateManager when something changes.
            var state = Game.Instance.StateManager;

            _lastKeyboard = Keyboard;
            _lastKeyboardKeys = KeyboardKeys;
            Keyboard = Microsoft.Xna.Framework.Input.Keyboard.GetState();
            KeyboardKeys = new HashSet<Keys>(Keyboard.GetPressedKeys());

            var lshift = Keyboard.IsKeyDown(Keys.LeftShift);
            var rshift = Keyboard.IsKeyDown(Keys.RightShift);
            var shift = lshift || rshift;

            var lctrl = Keyboard.IsKeyDown(Keys.LeftControl);
            var rctrl = Keyboard.IsKeyDown(Keys.RightControl);
            var ctrl = lctrl || rctrl;

            var lalt = Keyboard.IsKeyDown(Keys.LeftAlt);
            var ralt = Keyboard.IsKeyDown(Keys.RightAlt);
            var alt = lalt || ralt;

            foreach (Keys k in _lastKeyboardKeys)
            {
                if (!KeyboardKeys.Contains(k))
                {
                    Logger.Trace(m => m("KeyUp: {0}", k));
                    state.KeyUp(k);
                }
            }
            foreach (Keys k in KeyboardKeys)
            {
                if (!_lastKeyboardKeys.Contains(k))
                {
                    Logger.Trace(m => m("KeyDown: {0}", k));
                    state.KeyDown(k, shift, ctrl, alt);
                }
            }

            _lastMouse = Mouse;
            Mouse = Microsoft.Xna.Framework.Input.Mouse.GetState();

            Point position = new Point(Mouse.X, Mouse.Y);
            Point mouseDelta = new Point(position.X - _lastMouse.X, position.Y - _lastMouse.Y);
            if (mouseDelta.X != 0 || mouseDelta.Y != 0)
            {
                state.MouseMove(position, mouseDelta);
            }

            Int32 wheelChange = Math.Sign(Mouse.ScrollWheelValue.CompareTo(_lastMouse.ScrollWheelValue));
            if (wheelChange != 0)
                state.MouseWheel(position, wheelChange);

            if (_lastMouse.LeftButton == ButtonState.Pressed && Mouse.LeftButton == ButtonState.Released)
                state.MouseUp(position, MouseButton.Left);
            else if (_lastMouse.LeftButton == ButtonState.Released && Mouse.LeftButton == ButtonState.Pressed)
                state.MouseDown(position, MouseButton.Middle, shift, ctrl, alt);

            if (_lastMouse.MiddleButton == ButtonState.Pressed && Mouse.MiddleButton == ButtonState.Released)
                state.MouseUp(position, MouseButton.Middle);
            else if (_lastMouse.MiddleButton == ButtonState.Released && Mouse.MiddleButton == ButtonState.Pressed)
                state.MouseDown(position, MouseButton.Middle, shift, ctrl, alt);

            if (_lastMouse.RightButton == ButtonState.Pressed && Mouse.RightButton == ButtonState.Released)
                state.MouseUp(position, MouseButton.Right);
            else if (_lastMouse.RightButton == ButtonState.Released && Mouse.RightButton == ButtonState.Pressed)
                state.MouseDown(position, MouseButton.Right, shift, ctrl, alt);

            if (_lastMouse.XButton1 == ButtonState.Pressed && Mouse.XButton1 == ButtonState.Released)
                state.MouseUp(position, MouseButton.XButton1);
            else if (_lastMouse.XButton1 == ButtonState.Released && Mouse.XButton1 == ButtonState.Pressed)
                state.MouseDown(position, MouseButton.XButton1, shift, ctrl, alt);

            if (_lastMouse.XButton2 == ButtonState.Pressed && Mouse.XButton2 == ButtonState.Released)
                state.MouseUp(position, MouseButton.XButton2);
            else if (_lastMouse.XButton2 == ButtonState.Released && Mouse.XButton2 == ButtonState.Pressed)
                state.MouseDown(position, MouseButton.XButton2, shift, ctrl, alt);


            for (Int32 i = 0; i < 4; ++i)
            {
                PlayerIndex playerIndex = ArrayIndexToPlayerIndex(i);

                _lastControllerStates[i] = ControllerStates[i];
                ControllerStates[i] = GamePad.GetState(playerIndex);

                var s1 = _lastControllerStates[i];
                var s2 = ControllerStates[i];

                var tLD = s2.ThumbSticks.Left - s1.ThumbSticks.Left;
                if (tLD.X != 0 || tLD.Y != 0)
                    state.GamePadThumbstickMove(playerIndex, Thumbstick.Left, s2.ThumbSticks.Left, tLD);

                var tRD = s2.ThumbSticks.Right - s1.ThumbSticks.Right;
                if (tRD.X != 0 || tRD.Y != 0)
                    state.GamePadThumbstickMove(playerIndex, Thumbstick.Right, s2.ThumbSticks.Right, tRD);

                var rLD = s2.Triggers.Left - s1.Triggers.Left;
                if (rLD != 0)
                        state.GamePadTriggerMove(playerIndex, Trigger.Left, s2.Triggers.Left, rLD);

                var rRD = s2.Triggers.Right - s1.Triggers.Right;
                if (rRD != 0)
                    state.GamePadTriggerMove(playerIndex, Trigger.Right, s2.Triggers.Right, rRD);

                var b1 = s1.Buttons;
                var b2 = s2.Buttons;

                if (b1.A == ButtonState.Pressed && b2.A == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.A);
                if (b1.A == ButtonState.Released && b2.A == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.A);
                if (b1.B == ButtonState.Pressed && b2.B == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.B);
                if (b1.B == ButtonState.Released && b2.B == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.B);
                if (b1.Back == ButtonState.Pressed && b2.Back == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.Back);
                if (b1.Back == ButtonState.Released && b2.Back == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.Back);
                if (b1.BigButton == ButtonState.Pressed && b2.BigButton == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.BigButton);
                if (b1.BigButton == ButtonState.Released && b2.BigButton == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.BigButton);
                if (b1.LeftShoulder == ButtonState.Pressed && b2.LeftShoulder == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.LeftShoulder);
                if (b1.LeftShoulder == ButtonState.Released && b2.LeftShoulder == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.LeftShoulder);
                if (b1.LeftStick == ButtonState.Pressed && b2.LeftStick == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.LeftStick);
                if (b1.LeftStick == ButtonState.Released && b2.LeftStick == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.LeftStick);
                if (b1.RightShoulder == ButtonState.Pressed && b2.RightShoulder == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.RightShoulder);
                if (b1.RightShoulder == ButtonState.Released && b2.RightShoulder == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.RightShoulder);
                if (b1.RightStick == ButtonState.Pressed && b2.RightStick == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.RightStick);
                if (b1.RightStick == ButtonState.Released && b2.RightStick == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.RightStick);
                if (b1.Start == ButtonState.Pressed && b2.Start == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.Start);
                if (b1.Start == ButtonState.Released && b2.Start == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.Start);
                if (b1.X == ButtonState.Pressed && b2.X == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.X);
                if (b1.X == ButtonState.Released && b2.X == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.X);
                if (b1.Y == ButtonState.Pressed && b2.Y == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.Y);
                if (b1.Y == ButtonState.Released && b2.Y == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.Y);

                var d1 = s1.DPad;
                var d2 = s2.DPad;

                if (d1.Up == ButtonState.Pressed && d2.Up == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.DPadUp);
                if (d1.Up == ButtonState.Released && d2.Up == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.DPadUp);
                if (d1.Down == ButtonState.Pressed && d2.Down == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.DPadDown);
                if (d1.Down == ButtonState.Released && d2.Down == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.DPadDown);
                if (d1.Left == ButtonState.Pressed && d2.Left == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.DPadLeft);
                if (d1.Left == ButtonState.Released && d2.Left == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.DPadLeft);
                if (d1.Right == ButtonState.Pressed && d2.Right == ButtonState.Released)
                    state.GamePadButtonUp(playerIndex, Buttons.DPadRight);
                if (d1.Right == ButtonState.Released && d2.Right == ButtonState.Pressed)
                    state.GamePadButtonDown(playerIndex, Buttons.DPadRight);

              
            }
        }


        public static Int32 PlayerIndexToArrayIndex(PlayerIndex index)
        {
            switch (index)
            {
                case PlayerIndex.One:
                    return 0;
                case PlayerIndex.Two:
                    return 1;
                case PlayerIndex.Three:
                    return 2;
                case PlayerIndex.Four:
                    return 3;
                default:
                    throw new Exception("can't happen");
            }
        }

        public static PlayerIndex ArrayIndexToPlayerIndex(Int32 idx)
        {
            switch (idx)
            {
                case 0:
                    return PlayerIndex.One;
                case 1:
                    return PlayerIndex.Two;
                case 2:
                    return PlayerIndex.Three;
                case 3:
                    return PlayerIndex.Four;
                default:
                    throw new Exception("bad array index");
            }
        }
    }
}

