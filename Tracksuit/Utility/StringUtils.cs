﻿using System;

namespace Tracksuit.Utility
{
    public static class StringUtils
    {
        public static String toString<T>(T obj)
            where T : class
        {
            return (obj != null) ? obj.ToString() : "null";
        }
    }
}

