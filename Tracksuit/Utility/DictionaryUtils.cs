﻿using System;
using System.Collections.Generic;

namespace Tracksuit.Utility
{
    public static class DictionaryUtils
    {
        public static Dictionary<TKey, TValue> AsFallbackFor<TKey, TValue>(this Dictionary<TKey, TValue> dict, Dictionary<TKey, TValue> other)
        {
            Dictionary<TKey, TValue> newDict = new Dictionary<TKey, TValue>(dict);
            foreach (TKey key in other.Keys)
            {
                newDict.Remove(key);
                newDict.Add(key, other[key]);
            }
            return newDict;
        }
    }
}

