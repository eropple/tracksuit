﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Utility.Collections
{
    public class ReadOnlySet<TCollectionType> : ISet<TCollectionType>
    {
        private readonly ISet<TCollectionType> _underlying;

        public ReadOnlySet(ISet<TCollectionType> underlying)
        {
            _underlying = underlying;
        }

        public IEnumerator<TCollectionType> GetEnumerator()
        {
            return _underlying.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _underlying).GetEnumerator();
        }

        public void Add(TCollectionType item)
        {
            throw new InvalidOperationException("Readonly collection.");
        }

        bool ISet<TCollectionType>.Add(TCollectionType item)
        {
            throw new InvalidOperationException("Readonly collection.");
        }

        public void UnionWith(IEnumerable<TCollectionType> other)
        {
            throw new InvalidOperationException("Readonly collection.");
        }

        public void IntersectWith(IEnumerable<TCollectionType> other)
        {
            throw new InvalidOperationException("Readonly collection.");
        }

        public void ExceptWith(IEnumerable<TCollectionType> other)
        {
            throw new InvalidOperationException("Readonly collection.");
        }

        public void SymmetricExceptWith(IEnumerable<TCollectionType> other)
        {
            throw new InvalidOperationException("Readonly collection.");
        }

        public bool IsSubsetOf(IEnumerable<TCollectionType> other)
        {
            return _underlying.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<TCollectionType> other)
        {
            return _underlying.IsSupersetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<TCollectionType> other)
        {
            return _underlying.IsProperSupersetOf(other);
        }

        public bool IsProperSubsetOf(IEnumerable<TCollectionType> other)
        {
            return _underlying.IsProperSubsetOf(other);
        }

        public bool Overlaps(IEnumerable<TCollectionType> other)
        {
            return _underlying.Overlaps(other);
        }

        public bool SetEquals(IEnumerable<TCollectionType> other)
        {
            return _underlying.SetEquals(other);
        }

        public void Clear()
        {
            throw new InvalidOperationException("Readonly collection.");
        }

        public bool Contains(TCollectionType item)
        {
            return _underlying.Contains(item);
        }

        public void CopyTo(TCollectionType[] array, int arrayIndex)
        {
            _underlying.CopyTo(array, arrayIndex);
        }

        public bool Remove(TCollectionType item)
        {
            throw new InvalidOperationException("Readonly collection.");
        }

        public int Count
        {
            get { return _underlying.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }
    }
}
