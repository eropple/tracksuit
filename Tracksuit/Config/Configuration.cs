﻿using System;

namespace Tracksuit.Config
{
    public static class Configuration
    {
#if DESKTOP
        public static readonly DesktopConfiguration Instance;
        static Configuration()
        {
            Instance = new DesktopConfiguration();
        }
#else
#error Must implement.
#endif
    }
}