﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Config
{
    public interface IConfiguration
    {
        String GetString(String key, String defaultValue);
        Int32 GetInt32(String key, Int32 defaultValue);
        Single GetSingle(String key, Single defaultValue);
        Boolean GetBoolean(String key, Boolean defaultValue);
        IEnumerable<String> GetStringList(String key, IEnumerable<String> defaultValue);
    }

    public interface IMutableConfiguration : IConfiguration
    {
        void SetString(String key, String value);
        void SetInt32(String key, Int32 value);
        void SetSingle(String key, Single value);
        void SetStringList(String key, IEnumerable<String> value);

        void Save();
    }
}
