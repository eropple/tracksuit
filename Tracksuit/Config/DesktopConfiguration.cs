﻿#if DESKTOP
using System;
using System.Linq;
using Microsoft.Xna.Framework;
using NDesk.Options;
using NDesk.Options.Extensions;
using Common.Logging;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Collections.ObjectModel;

namespace Tracksuit.Config
{
    public class DesktopConfiguration : IMutableConfiguration
    {
        private static readonly ILog Logger = LogManager.GetCurrentClassLogger();

        private readonly Dictionary<String, String> defaults; 
        private readonly Dictionary<String, String> entries = new Dictionary<string, string>();

        public DesktopConfiguration()
        {
            foreach (String path in DefaultConfigSearchPaths)
                LoadEntries(path, entries, false);

            defaults = new Dictionary<string, string>(entries);

            LoadEntries(Path.Combine(SavedConfigPath, "user.conf"), entries, false);
        }

        public string GetString(string key, string defaultValue)
        {
            String value;
            return entries.TryGetValue(key, out value) ? value : defaultValue;
        }

        public int GetInt32(string key, int defaultValue)
        {
            String value;
            return entries.TryGetValue(key, out value) ? Int32.Parse(value): defaultValue;
        }

        public float GetSingle(string key, float defaultValue)
        {
            String value;
            return entries.TryGetValue(key, out value) ? Single.Parse(value) : defaultValue;
        }

        public IEnumerable<string> GetStringList(string key, IEnumerable<string> defaultValue)
        {
            String value;
            return entries.TryGetValue(key, out value) ? value.Split(';') : defaultValue;
        }

        public Boolean GetBoolean(String key, Boolean defaultValue)
        {
            String value;
            return entries.TryGetValue(key, out value)
                ? value == "yes" || value == "1" || value == "true"
                : defaultValue;
        }

        public void SetString(string key, string value)
        {
            entries.Remove(key);
            entries[key] = value.Trim();
        }

        public void SetInt32(string key, int value)
        {
            entries.Remove(key);
            entries[key] = value.ToString();
        }

        public void SetSingle(string key, float value)
        {
            entries.Remove(key);
            entries[key] = value.ToString();
        }

        public void SetBoolean(String key, Boolean value)
        {
            entries.Remove(key);
            entries[key] = value.ToString();
        }

        public void SetStringList(string key, IEnumerable<string> value)
        {
            entries.Remove(key);
            entries[key] = String.Join(";", value.Select(t => t.Trim()));
        }

        public void Save()
        {
            List<String> lines = new List<string>();
            foreach (var kvp in entries)
            {
                String defaultValue;
                if (defaults.TryGetValue(kvp.Key, out defaultValue))
                {
                    if (defaultValue.Trim() == kvp.Value) continue;
                }
                lines.Add(String.Format("{0} = {1}", kvp.Key, kvp.Value));
            }

            File.WriteAllLines(Path.Combine(SavedConfigPath, "user.conf"), lines);
        }


        private static List<String> DefaultConfigSearchPaths
        {
            get
            {
                var list = new List<String>(5);
#if MONOMAC
                    try
                    {
                        var bundle = MonoMac.Foundation.NSBundle.MainBundle;
                        if (bundle != null)
                        {
                            list.Add(Path.Combine(bundle.ResourcePath, "defaults.conf"));
                            list.Add(Path.Combine(bundle.ResourcePath, "osx.conf"));
                        }
                        else throw new Exception();
                    }
                    catch (Exception ex)
                    {
                        var root = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                        list.Add(Path.Combine(root, "defaults.conf"));
                        list.Add(Path.Combine(root, "osx.conf"));
                    }
#elif WINDOWS
                var root = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                list.Add(Path.Combine(root, "defaults.conf"));
                list.Add(Path.Combine(root, "windows.conf"));
#else
#error TODO: Must be defined.
#endif
                return list;
            }
        }

        public static String SavedConfigPath
        {
            get
            {
#if MONOMAC
                    return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Library", "Preferences", Tracksuit.Game.Instance.Name);
#elif WINDOWS
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Tracksuit.Game.Instance.Name);
#else
#error TODO: Must be defined.
#endif
            }
        }

        private static void LoadEntries(String path, Dictionary<String, String> entries, Boolean errorIfNotFound)
        {
            if (!File.Exists(path))
            {
                String message = String.Format("Config file '{0}' not found, skipping.", path);
                if (errorIfNotFound)
                    Logger.ErrorFormat(message);
                else
                    Logger.InfoFormat(message);
                return;
            }

            String[] lines = File.ReadAllLines(path);
            for (Int32 i = 0; i < lines.Length; ++i)
            {
                String line = lines[i].Trim();
                if (line.Length == 0 || line[0] == '#' || line[0] == ';')
                    continue;


                if (!line.Contains("="))
                {
                    Logger.Warn(m => m("Malformed line {0} in '{1}': no equals sign.", i, path));
                    continue;
                }

                String[] tokens = line.Split(new[] { '=' }, 2);
                String key = tokens[0].Trim().ToLower();
                String value = tokens[1].Trim();

                Logger.Trace(m => m("Config from '{0}': '{1}' = '{2}'", path, key, value));

                entries.Remove(key);
                entries.Add(key, value);
            }
        }
    }
}
#endif // DESKTOP
