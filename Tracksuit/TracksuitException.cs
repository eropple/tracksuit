﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit
{
    public class TracksuitException : Exception
    {
        public TracksuitException(String message, Exception inner = null) : base(message, inner)
        {
        }

        public TracksuitException(String message, params Object[] formatObjects)
            : base(String.Format(message, formatObjects), null)
        {
        }

        public TracksuitException(Exception inner, String message, params Object[] formatObjects)
            : base(String.Format(message, formatObjects), inner)
        {
        }
    }
}
