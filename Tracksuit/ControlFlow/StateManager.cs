﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Tracksuit.Content;
using Tracksuit.Graphics.Fonts;
using Tracksuit.Input;
using Common.Logging;
using Tracksuit.Utility;

namespace Tracksuit.ControlFlow
{
    public class StateManager
    {
        private readonly ILog Logger = LogManager.GetCurrentClassLogger();

        private readonly List<State> _states;
        private readonly HashSet<State> _killSet;

        private Boolean _stateStackChanged = false;
#if DEBUG
        private Boolean _debug = true;
#else
        private Boolean _debug = false;
#endif

        private SpriteBatch _batch = null;
        private BitmapFont _debugFont;

        public StateManager()
        {
            _states = new List<State>(20);
            _killSet = new HashSet<State>();
        }

        public void Push(State newState)
        {
            State old = _states.Count > 0 ? _states[_states.Count - 1] : null;

            Logger.Info(m => m("Pushing {0}.", newState));

            newState.DoLoadContent();
            _states.Add(newState);
            newState.DoInitialize();

            _killSet.Remove(newState);

            if (old != null)
                old.NoLongerTopState(newState);
        }

        public State Pop()
        {
            State old = _states.Count > 0 ? _states[_states.Count - 1] : null;

            if (old == null)
                throw new Exception("Tried to pop with no states.");

            State newTop = _states.Count > 1 ? _states[_states.Count - 2] : null;

            Logger.Info(m => m("Popping {0} over {1}.", newTop, StringUtils.toString(old)));

            _states.RemoveAt(_states.Count - 1);

            if (newTop != null)
                newTop.BecameTopState(old);

            _killSet.Add(old);

            return old;
        }

        public Boolean Remove(State state)
        {
            State top = _states.Count > 0 ? _states[_states.Count - 1] : null;
            if (state == top)
            {
                Pop();
                return true;
            }

            if (_states.Remove(state))
            {
                _stateStackChanged = true;
                Logger.Warn(m => m("Removed {0} from the state stack (not on top)."));
                return true;
            }
            Logger.Warn(m => m("Attempted to remove {0}, but it's not in the state stack.", state));
            return false;
        }


        public void Update(GameTime gameTime)
        {
            if (_states.Count == 0)
                throw new Exception("Cannot call Update() with a state count of zero.");

            _stateStackChanged = false;

            Int32 idx = _states.FindLastIndex((State obj) => obj.SoaksUpdates);
            if (idx == -1)
                idx = 0;



            State current = null;

            Int64 delta = (Int64)Math.Round(gameTime.ElapsedGameTime.TotalMilliseconds);

            restartLoop:
            for (Int32 j = idx; j < _states.Count; ++j)
            {
                current = _states[j];

                current.DoUpdate(delta, j == _states.Count - 1, _debug);

                // So this is kind of tough. If the state stack has changed, we need to find the next one and resume.
                // What makes this particularly aggravating is that Mono only does the list length for the for loop
                // once and caches the result, so we have to break the loop and try again.
                if (_stateStackChanged)
                {
                    if (_killSet.Contains(current) || _states[j] != current) // in the weird case of a remove+push
                    {
                        idx = j; // we don't increment the counter, so this will retry at the same index
                        _stateStackChanged = false;
                        goto restartLoop;
                    }
                    else if (!_killSet.Contains(current))
                    {
                        idx = j + 1; // we need to increment the counter
                        _stateStackChanged = false;
                        goto restartLoop;
                    }
                }
            }

            foreach (State s in _killSet)
                s.Dispose();

            _stateStackChanged = false;
        }

        public void Draw(GameTime gameTime)
        {
            Game.Instance.GraphicsDevice.Clear(Color.Black);

            if (_batch == null)
            {
                _batch = new SpriteBatch(Game.Instance.GraphicsDevice);
            }

            Int32 idx = _states.FindLastIndex((State obj) => obj.SoaksDraws);
            if (idx == -1)
                idx = 0;

            Int64 delta = (Int64)Math.Round(gameTime.ElapsedGameTime.TotalMilliseconds);
            for (Int32 j = idx; j < _states.Count; ++j)
            {
                if (_stateStackChanged)
                    throw new Exception("Cannot modify the state stack during drawing.");

                _states[j].DoDraw(_batch, delta, j == _states.Count - 1, _debug);
            }

            if (_debug)
            {
                DrawGlobalOverlay();
            }
        }

        private void DrawGlobalOverlay()
        {
            if (_debugFont == null)
            {
                _debugFont = BitmapFont.DebugFont;
            }

            _batch.Begin();

            _debugFont.BuildTextSource(String.Format("{1} states: {0}", _states.Last(), _states.Count), Color.White).Draw(_batch, new Vector2(2, 2));

            _batch.End();
        }



        public void KeyUp(Keys key)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].KeyUp(key) || _states[i].SoaksInput)
                    return;
        }
        public void KeyDown(Keys key, Boolean shift, Boolean ctrl, Boolean alt)
        {
            if (key == Keys.F6)
            {
                Logger.Info(m => m("Attempting to toggle full screen."));
                Game.Instance.GraphicsDeviceManager.ToggleFullScreen();
                return;
            }
            if (key == Keys.F8)
            {
                Logger.Info(m => m("Toggling debug information."));
                _debug = !_debug;
                return;
            }
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].KeyDown(key, shift, ctrl, alt) || _states[i].SoaksInput)
                    return;
        }

        public void MouseMove(Point position, Point delta)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].MouseMove(position, delta) || _states[i].SoaksInput)
                    return;
        }
        public void MouseWheel(Point position, Int32 direction)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].MouseWheel(position, direction) || _states[i].SoaksInput)
                    return;
        }
        public void MouseUp(Point position, MouseButton button)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].MouseUp(position, button) || _states[i].SoaksInput)
                    return;
        }
        public void MouseDown(Point position, MouseButton button, Boolean shift, Boolean ctrl, Boolean alt)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].MouseDown(position, button, shift, ctrl, alt) || _states[i].SoaksInput)
                    return;
        }

        public void GamePadButtonUp(PlayerIndex index, Buttons button)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].GamePadButtonUp(index, button) || _states[i].SoaksInput)
                    return;
        }
        public void GamePadButtonDown(PlayerIndex index, Buttons button)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].GamePadButtonDown(index, button) || _states[i].SoaksInput)
                    return;
        }
        public void GamePadThumbstickMove(PlayerIndex index, Thumbstick thumbstick, Vector2 position, Vector2 delta)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].GamePadThumbstickMove(index, thumbstick, position, delta) || _states[i].SoaksInput)
                    return;
        }
        public void GamePadTriggerMove(PlayerIndex index, Trigger trigger, Single position, Single delta)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].GamePadTriggerMove(index, trigger, position, delta) || _states[i].SoaksInput)
                    return;
        }
    }
}

