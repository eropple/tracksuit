﻿using System;
using Tracksuit.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Tracksuit.Input;
using Common.Logging;
using Tracksuit.Tweening;

namespace Tracksuit.ControlFlow
{
    public abstract class State : IExtendedDisposable
    {
        protected readonly ILog Logger;

        public readonly Int32 ID;
        public readonly TweenController Tweens = new TweenController();
        protected TSContentManager ContentManager { get; private set; }
        protected StateManager StateManager { get; private set; }
        public Boolean IsInitialized { get; private set; }
        public Boolean IsContentLoaded { get; private set; }
        public Boolean IsDisposed { get; private set; }

        public Boolean SoaksUpdates { get; protected set; }
        public Boolean SoaksDraws { get; protected set; }
        public Boolean SoaksInput { get; protected set; }

        public GraphicsDevice GraphicsDevice { get; private set; }

        protected State()
        {
            Logger = LogManager.GetCurrentClassLogger();
            ID = GetIDAndIncrement();

            IsInitialized = false;
            IsDisposed = false;

            SoaksUpdates = false;
            SoaksDraws = false;
            SoaksInput = false;
        }


        public void DoLoadContent()
        {
            if (IsContentLoaded)
                return;

            StateManager = Game.Instance.StateManager;
            ContentManager = new TSContentManager(TSContentManager.Global);
            GraphicsDevice = Game.Instance.GraphicsDevice;

            Logger.Debug(m => m("Loading content for #{0}.", ID));
            LoadContent();
        }

        public void DoInitialize()
        {
            if (IsInitialized)
                return;

            Logger.Debug(m => m("Initializing #{0}.", ID));
            Initialize();
            IsInitialized = true;
        }
        protected abstract void LoadContent();
        protected abstract void Initialize();

        public void DoUpdate(Int64 delta, Boolean topOfStack, Boolean debug)
        {
            Tweens.Update(delta);
            Update(delta, topOfStack, debug);
        }
        protected abstract void Update(Int64 delta, Boolean topOfStack, Boolean debug);

        public void DoDraw(SpriteBatch batch, Int64 delta, Boolean topOfStack, Boolean debug)
        {
            Draw(batch, delta, topOfStack, debug);
        }
        protected abstract void Draw(SpriteBatch batch, Int64 delta, Boolean topOfStack, Boolean debug);


        public virtual void BecameTopState(State oldTopState) {}
        public virtual void NoLongerTopState(State newTopState) {}


        public virtual Boolean KeyUp(Keys key) { return false; }
        public virtual Boolean KeyDown(Keys key, Boolean shift, Boolean ctrl, Boolean alt) { return false; }

        public virtual Boolean MouseMove(Point position, Point delta) { return false; }
        public virtual Boolean MouseWheel(Point position, Int32 direction) { return false; }
        public virtual Boolean MouseUp(Point position, MouseButton button) { return false; }
        public virtual Boolean MouseDown(Point position, MouseButton button, Boolean shift, Boolean ctrl, Boolean alt) { return false; }

        public virtual Boolean GamePadButtonUp(PlayerIndex index, Buttons button) { return false; }
        public virtual Boolean GamePadButtonDown(PlayerIndex index, Buttons button) { return false; }
        public virtual Boolean GamePadThumbstickMove(PlayerIndex index, Thumbstick thumbstick, Vector2 position, Vector2 delta) { return false; }
        public virtual Boolean GamePadTriggerMove(PlayerIndex index, Trigger trigger, Single position, Single delta) { return false; }

        public override string ToString()
        {
            return String.Format("{0} (#{1})", this.GetType().Name, ID);
        }

        #region IDisposable implementation

        public void Dispose()
        {
            if (IsDisposed)
                return;
            Destroy();
            IsDisposed = true;
        }
        protected virtual void Destroy() {}

        #endregion





        private static readonly Object _managerIdLock = new Object();
        private static Int32 _managerId = 0;

        private static Int32 GetIDAndIncrement()
        {
            lock (_managerIdLock)
            {
                return _managerId++;
            }
        }
    }
}

