﻿using Tracksuit.Config;
using Tracksuit.Content;
using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracksuit.Utility;

namespace Tracksuit.Internationalization
{
    public static class I18N
    {
        private static readonly ILog Logger = LogManager.GetCurrentClassLogger();

        private static readonly Dictionary<String, String> _translations = new Dictionary<String, String>(5000);

        public static String Translate(String key, params Object[] options)
        {
            String value;
            if (_translations.TryGetValue(key, out value)) return value;

            return "X: " + key;
        }

        static I18N()
        {
            foreach (String bundleName in Configuration.Instance.GetStringList("bundles.i18n", "general".Yield()))
            {
                Logger.Debug(m => m("For bundle '{0}'...", bundleName));
                foreach (String langName in Configuration.Instance.GetStringList("languages", "en".Yield()))
                {
                    Logger.Debug(m => m("For language '{0}'...", langName));
                    String path = String.Format("I18N/{0}/{1}.i18n-xml", langName, bundleName);

                    try
                    {
                        I18NBundle[] bundles = TSContentManager.Global.LoadAll<I18NBundle>(path);
                        foreach (var bundle in bundles.Reverse())
                        {
                            if (Configuration.Instance.GetBoolean("languages.debug", false))
                            {
                                foreach (var tuple in bundle.Entries)
                                {
                                    _translations[tuple.Item1] = String.Format("({0}) {1}", langName, tuple.Item2);
                                }
                            }
                            else
                            {
                                foreach (var tuple in bundle.Entries)
                                {
                                    _translations[tuple.Item1] = tuple.Item2;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Logger.Warn(m => m("Could not load language bundle '{0}'.", path));
                    }
                }
            }
        }
    }
}
