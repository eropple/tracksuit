﻿using Tracksuit.Content;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Internationalization
{
    public class I18NBundle
    {
        public readonly ReadOnlyCollection<Tuple<String, String>> Entries;

        public I18NBundle(IList<Tuple<String, String>> entries)
        {
            Entries = new ReadOnlyCollection<Tuple<string, string>>(entries);
        }
    }

    public class I18NBundleReader : BaseContentTypeReader<I18NBundle>
    {
        protected override object Read(Microsoft.Xna.Framework.Content.ContentReader input, object existingInstance)
        {
            Int32 count = input.ReadInt32();
            List<Tuple<String, String>> entries = new List<Tuple<string, string>>(count);

            for (Int32 i = 0; i < count; ++i)
            {
                entries.Add(Tuple.Create(input.ReadString(), input.ReadString()));
            }
            
            return new I18NBundle(entries);
        }
    }
}
