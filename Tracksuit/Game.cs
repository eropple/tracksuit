﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Common.Logging;
using Tracksuit.ControlFlow;
using Tracksuit.Input;
using Tracksuit.Config;
using Tracksuit.Content;


namespace Tracksuit
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public sealed class Game : Microsoft.Xna.Framework.Game
    {
        private readonly ILog Logger = LogManager.GetCurrentClassLogger();
        public readonly GraphicsDeviceManager GraphicsDeviceManager;
        public readonly String Name;
        public StateManager StateManager { get; private set; }
        public InputManager InputManager { get; private set; }

        private readonly Func<Int32, State> _firstStateFunction;

        public Game(String name, String title, Func<Int32, State> firstState)
        {
            if (Instance != null)
                throw new InvalidProgramException("Cannot duplicate Game class.");

            Instance = this;

            Name = name;
            GraphicsDeviceManager = new GraphicsDeviceManager(this);

            var config = Configuration.Instance;
            GraphicsDeviceManager.SupportedOrientations = DisplayOrientation.LandscapeLeft;
            GraphicsDeviceManager.PreferredBackBufferWidth = config.GetInt32("graphics.window.x", 1280);
            GraphicsDeviceManager.PreferredBackBufferHeight = config.GetInt32("graphics.window.y", 720);
            GraphicsDeviceManager.IsFullScreen = config.GetBoolean("graphics.fullscreen", false);

            GraphicsDeviceManager.SynchronizeWithVerticalRetrace = true;
            GraphicsDeviceManager.PreferMultiSampling = false;

            this.Window.Title = title;
            this.IsMouseVisible = true;

            _firstStateFunction = firstState;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            TSContentManager.Initialize();
            InputManager = new InputManager();
            StateManager = new StateManager();
            StateManager.Push(_firstStateFunction(0));
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            Logger.Trace(m => m("Update starts: gameTime = {0} ms", gameTime.ElapsedGameTime.TotalMilliseconds));

            #if DEBUG
                if (Logger.IsTraceEnabled)
                {
                    DateTime start = DateTime.UtcNow;

                    base.Update(gameTime);
                    InputManager.Update();
                    StateManager.Update(gameTime);

                    DateTime end = DateTime.UtcNow;
                    Logger.Trace(m => m("Update ends: duration = {0} ms", (end - start).TotalMilliseconds));
                }
                else
                {
            #endif // DEBUG
                    base.Update(gameTime);
                    InputManager.Update();
                    StateManager.Update(gameTime);
            #if DEBUG
                }
            #endif // DEBUG
        }
        /// <summary>
        /// Draws the game from background to foreground.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            Logger.Trace(m => m("Draw starts: gameTime = {0} ms", gameTime.ElapsedGameTime.TotalMilliseconds));

            #if DEBUG
                if (Logger.IsTraceEnabled)
                {
                    DateTime start = DateTime.UtcNow;

                    base.Draw(gameTime);
                    StateManager.Draw(gameTime);

                    DateTime end = DateTime.UtcNow;
                    Logger.Trace(m => m("Draw ends: duration = {0} ms", (end - start).TotalMilliseconds));
                }
                else
                {
            #endif // DEBUG
                    base.Draw(gameTime);
                    StateManager.Draw(gameTime);
            #if DEBUG
                }
            #endif // DEBUG
        }




        public static Game Instance = null;
    }
}
