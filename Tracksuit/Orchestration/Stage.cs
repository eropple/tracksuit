﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracksuit.Tweening;
using Tracksuit.Utility.Collections;

namespace Tracksuit.Orchestration
{
    public class Stage
    {
        private readonly HashSet<Actor> _actors;
        public readonly ReadOnlySet<Actor> Actors;
        public readonly TweenController TweenController = new TweenController();

        public Stage()
        {
            _actors = new HashSet<Actor>();
            Actors = new ReadOnlySet<Actor>(_actors);
        }

        public void Add(Actor item)
        {
            _actors.Add(item);
        }

        public bool Remove(Actor item)
        {
            return _actors.Remove(item);
        }
    }
}
