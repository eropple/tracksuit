﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Tracksuit.Tweening;

namespace Tracksuit.Orchestration
{
    public abstract class Actor
    {
        public Stage Stage { get; internal set; }
        public Vector2 Position { get; internal set; }
    }
}
