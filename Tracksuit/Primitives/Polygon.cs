﻿using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracksuit.Content;

namespace Tracksuit.Primitives
{
    public class Polygon : IEnumerable<Vector2>
    {
        public readonly ReadOnlyCollection<Vector2> Vertices;

        public Polygon(List<Vector2> vertices)
        {
            Vertices = new ReadOnlyCollection<Vector2>(vertices);
        }

        public IEnumerator<Vector2> GetEnumerator()
        {
            return Vertices.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Vertices.GetEnumerator();
        }

        public static implicit operator Vertices(Polygon p)
        {
            return new Vertices(p);
        }
    }

    public class PolygonReader : BaseContentTypeReader<Polygon>
    {
        protected override object Read(Microsoft.Xna.Framework.Content.ContentReader input, object existingInstance)
        {
            Int32 count = input.ReadInt32();
            List<Vector2> v = new List<Vector2>(count);
            for (Int32 i = 0; i < count; ++i) v.Add(input.ReadVector2());
            return new Polygon(v);
        }
    }
}
