﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Tracksuit.Graphics
{
    public static class Anchors
    {
        public static readonly Vector2 TopLeft;
        public static readonly Vector2 MiddleLeft;
        public static readonly Vector2 BottomLeft;
        public static readonly Vector2 BottomCenter;
        public static readonly Vector2 BottomRight;
        public static readonly Vector2 MiddleRight;
        public static readonly Vector2 TopRight;
        public static readonly Vector2 TopCenter;
        public static readonly Vector2 Center;

        static Anchors()
        {
            Int32 height = Game.Instance.Window.ClientBounds.Height;
            Int32 width = Game.Instance.Window.ClientBounds.Width;

            TopLeft = Vector2.Zero;
            MiddleLeft = new Vector2(0, height / 2);
            BottomLeft = new Vector2(0, height);
            BottomCenter = new Vector2(width / 2, height);
            BottomRight = new Vector2(width, height);
            MiddleRight = new Vector2(width, height / 2);
            TopRight = new Vector2(width, 0);
            TopCenter = new Vector2(width / 2, 0);
            Center = new Vector2(width / 2, height / 2);
        }
    }
}
