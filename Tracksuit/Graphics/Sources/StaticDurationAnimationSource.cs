﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Graphics.Sources
{
    public class StaticDurationAnimationSource : IDrawSource
    {
        private readonly Int32 _duration;
        private readonly List<TextureRegionSource> _frames;

        public StaticDurationAnimationSource(Int32 duration, IEnumerable<TextureRegionSource> frames)
        {
            _duration = duration;
            _frames = new List<TextureRegionSource>(frames);
        }

        public DrawInfo GetDrawInfo(Int64 time, DrawSourceOptions options = 0)
        {
            Int32 frame = (options & DrawSourceOptions.AnimationClamps) != DrawSourceOptions.AnimationClamps
                ? (Int32) (time % (_frames.Count * _duration)) / _duration
                : (Int32) (Math.Max(time, _frames.Count * _duration) / _duration);

            return _frames[frame % _frames.Count].GetDrawInfo(0, 0);
        }
    }
}
