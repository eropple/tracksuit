﻿using System;

namespace Tracksuit.Graphics.Sources
{
    public interface IDrawSource
    {
        DrawInfo GetDrawInfo(Int64 time = 0, DrawSourceOptions options = 0);
    }
}

