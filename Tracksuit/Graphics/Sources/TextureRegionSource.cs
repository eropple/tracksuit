﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Tracksuit.Graphics.Sources
{
    public class TextureRegionSource : IDrawSource
    {
        protected readonly DrawInfo _info;

        public TextureRegionSource(Texture2D texture, Rectangle rectangle)
        {
            _info = new DrawInfo(texture, rectangle);
        }

        public DrawInfo GetDrawInfo(Int64 time = 0, DrawSourceOptions options = 0)
        {
            return _info;
        }
    }
}

