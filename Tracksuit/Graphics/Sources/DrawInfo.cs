﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Tracksuit.Graphics.Sources
{
    public struct DrawInfo
    {
        public readonly Texture2D Texture;
        public readonly Rectangle Rectangle;

        public DrawInfo(Texture2D texture, Rectangle rectangle)
        {
            Texture = texture;
            Rectangle = rectangle;
        }
    }
}

