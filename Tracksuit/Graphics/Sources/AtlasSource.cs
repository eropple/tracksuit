﻿using Tracksuit.Common.Collections;
using Tracksuit.Content;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Graphics.Sources
{
    public class AtlasSource : IDrawSource
    {
        public readonly TextureSource Source;
        public readonly ReadOnlyDictionary<String, TextureRegionSource> Entries;

        public AtlasSource(TextureSource source, Dictionary<String, TextureRegionSource> entries)
        {
            Source = source;
            Entries = new ReadOnlyDictionary<String, TextureRegionSource>(new Dictionary<String, TextureRegionSource>(entries));
        }

        public DrawInfo GetDrawInfo(Int64 time = 0, DrawSourceOptions options = 0)
        {
            return Source.GetDrawInfo(0, options);
        }
    }

    public class AtlasSourceReader : BaseContentTypeReader<AtlasSource>
    {
        protected override object Read(Microsoft.Xna.Framework.Content.ContentReader input, object existingInstance)
        {
            TextureSource source = input.ReadObject<TextureSource>();
            Int32 count = input.ReadInt32();
            var entries = new Dictionary<String, TextureRegionSource>(count);

            for (Int32 i = 0; i < count; ++i)
            {
                entries.Add(input.ReadString(), new TextureRegionSource(source.Info.Texture, input.ReadRectangle()));
            }

            return new AtlasSource(source, entries);
        }
    }
}
