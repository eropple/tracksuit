﻿using Tracksuit.Graphics.Fonts.Metrics;
using Tracksuit.Graphics.Sources;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Graphics.Fonts
{
    public class TextSource
    {
        public readonly BitmapFont Font;
        private readonly List<List<GlyphInfo>> _glyphs;

        internal TextSource(BitmapFont font, List<List<GlyphInfo>> glyphs)
        {
            Font = font;
            _glyphs = glyphs;
        }

        public void Draw(SpriteBatch batch, Vector2 offset)
        {
            if (Font.IsDisposed)
                throw new Exception("TextSource has outlived its BitmapFont.");

            for (Int32 lineNumber = 0; lineNumber < _glyphs.Count; ++lineNumber)
            {
                float y = lineNumber * Font.LineHeight + offset.Y;
                float xpos = offset.X;

                List<GlyphInfo> line = _glyphs[lineNumber];

                for (Int32 character = 0; character < line.Count; ++character)
                {
                    GlyphInfo glyph = line[character];
                    Int32 kern = 0;
                    if (glyph.Character != ' ')
                    {
                        DrawInfo draw = glyph.CharacterInfo.Region.GetDrawInfo(0);
                        if (character > 0)
                            kern = Font.GetKerningValue(Convert.ToInt32(line[character - 1].Character), Convert.ToInt32(line[character].Character)) ?? 0;

                        Vector2 position = new Vector2(xpos + kern + glyph.CharacterInfo.Offset.X, y + glyph.CharacterInfo.Offset.Y);

                        batch.Draw(draw.Texture, position, draw.Rectangle, glyph.Color);
                    }

                    xpos += glyph.CharacterInfo.XAdvance + kern;
                }
            }
        }

        public void Draw(BatchStacker batch)
        {
            Draw(batch.Batch, Vector2.Zero);
        }



        public class GlyphInfo
        {
            public readonly Char Character;
            public readonly CharacterInfo CharacterInfo;
            public readonly Color Color;

            public GlyphInfo(Char c, CharacterInfo info, Color color)
            {
                this.Character = c;
                this.CharacterInfo = info;
                this.Color = color;
            }

            public override string ToString()
            {
                return Character.ToString() + " " + Color.ToString();
            }
        }
    }
}
