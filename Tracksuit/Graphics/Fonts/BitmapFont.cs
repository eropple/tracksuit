﻿using Microsoft.Xna.Framework.Content;
using Tracksuit.Common.Collections;
using Tracksuit.Config;
using Tracksuit.Content;
using Tracksuit.Graphics.Fonts.Metrics;
using Tracksuit.Graphics.Sources;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GlyphInfo = Tracksuit.Graphics.Fonts.TextSource.GlyphInfo;

namespace Tracksuit.Graphics.Fonts
{
    /// <summary>
    /// Translation object to turn the BMFont stuff into a somewhat
    /// sane object format for our use.
    /// </summary>
    public class BitmapFont : IExtendedDisposable
    {
        protected static readonly Regex ShortColorCodeRegex = new Regex(@"^\{\#[a-fA-F0-9]{6}\}", RegexOptions.IgnoreCase);
        protected static readonly Regex ColorCodeRegex = new Regex(@"^\{\#[a-fA-F0-9]{8}\}", RegexOptions.IgnoreCase);
        protected static readonly Regex ColorResetRegex = new Regex(@"^\{\#x\}", RegexOptions.IgnoreCase);

        public readonly String Face;

        public readonly Int32 LineHeight;
        public readonly Int32 Baseline;
        public readonly ReadOnlyDictionary<Int32, TextureSource> Pages;

        public readonly ReadOnlyDictionary<Int32, CharacterInfo> CharacterTable;

        public readonly ReadOnlyDictionary<Int32, ReadOnlyDictionary<Int32, KerningInfo>> KerningTable;

        public Color DefaultColor { get; set; }

        public BitmapFont(String face, Int32 lineHeight, Int32 baseline, 
                        Dictionary<Int32, TextureSource> pages, List<CharacterInfo> chars,
                        List<KerningInfo> kernings)
        {
            this.DefaultColor = Color.White;

            this.Face = face;

            this.LineHeight = lineHeight;
            this.Baseline = baseline;

            this.Pages = new ReadOnlyDictionary<Int32, TextureSource>(pages);

            Dictionary<Int32, CharacterInfo> charDict = new Dictionary<int,CharacterInfo>(chars.Count);
            foreach (var c in chars) charDict.Add(c.ID, c);
            this.CharacterTable = new ReadOnlyDictionary<Int32, CharacterInfo>(charDict);

            Dictionary<Int32, Dictionary<Int32, KerningInfo>> kernDict = new Dictionary<Int32, Dictionary<Int32, KerningInfo>>();
            foreach (KerningInfo k in kernings)
            {
                Dictionary<Int32, KerningInfo> subDict = null;

                if (kernDict.TryGetValue(k.First, out subDict) == false)
                {
                    subDict = new Dictionary<Int32, KerningInfo>();
                    kernDict.Add(k.First, subDict);
                }
                
                subDict.Add(k.Second, k);
            }
            Dictionary<Int32, ReadOnlyDictionary<Int32, KerningInfo>> roDict = new Dictionary<Int32,ReadOnlyDictionary<Int32,KerningInfo>>();
            foreach (Int32 k in kernDict.Keys)
            {
                roDict.Add(k, new ReadOnlyDictionary<Int32, KerningInfo>(kernDict[k]));
            }
            this.KerningTable = new ReadOnlyDictionary<Int32, ReadOnlyDictionary<Int32, KerningInfo>>(roDict);
        }

        public Int32? GetKerningValue(Int32 a, Int32 b)
        {
            ReadOnlyDictionary<Int32, KerningInfo> subDict = null;

            if (this.KerningTable.TryGetValue(a, out subDict) == false)
            {
                return null;
            }

            KerningInfo k = null;

            if (subDict.TryGetValue(b, out k) == false)
            {
                return null;
            }

            return k.Amount;
        }

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            if (IsDisposed) return;
            foreach (TextureSource s in Pages.Values) s.Dispose();
            IsDisposed = true;
        }



        public TextSource BuildTextSource(String text, Color defaultColor, Int32 wrapWidth = -1)
        {
            // TODO: support \n
            Color currentColor = defaultColor;

            StringBuilder work = new StringBuilder(text);

            List<List<GlyphInfo>> lines = new List<List<GlyphInfo>>();

            List<GlyphInfo> line = new List<GlyphInfo>();
            Int32 lineLength = 0;
            List<GlyphInfo> word = new List<GlyphInfo>();
            Int32 wordLength = 0;

            while (work.Length > 0)
            {
                String code = null;

                if (ControlCheck(work.ToString(), out code) == true)
                {
                    if (code == "{#x}")
                    {
                        currentColor = defaultColor;
                    }
                    else if (code.Length == 9)
                    {
                        Byte r = Byte.Parse(code.Substring(2, 2), NumberStyles.HexNumber);
                        Byte g = Byte.Parse(code.Substring(4, 2), NumberStyles.HexNumber);
                        Byte b = Byte.Parse(code.Substring(6, 2), NumberStyles.HexNumber);
                        Byte a = Byte.MaxValue;

                        currentColor = new Color(r, g, b, a);
                    }
                    else if (code.Length == 11)
                    {
                        Byte r = Byte.Parse(code.Substring(2, 2), NumberStyles.HexNumber);
                        Byte g = Byte.Parse(code.Substring(4, 2), NumberStyles.HexNumber);
                        Byte b = Byte.Parse(code.Substring(6, 2), NumberStyles.HexNumber);
                        Byte a = Byte.Parse(code.Substring(8, 2), NumberStyles.HexNumber);

                        currentColor = new Color(r, g, b, a);
                    }

                    work.Remove(0, code.Length);

                    continue; // breaks to top of while
                }

                Char c = work[0];
                if (c == '\n')
                {
                    line.AddRange(word);
                    lineLength += wordLength;

                    lines.Add(line); // old line is archived

                    line = new List<GlyphInfo>(); // word becomes the new line (start of new line)
                    lineLength = 0; // carry over the value
                    word = new List<GlyphInfo>();
                    wordLength = 0;
                }
                else
                {
                    CharacterInfo charInfo = this.CharacterTable[Convert.ToInt32(c)];


                    word.Add(new GlyphInfo(c, charInfo, currentColor));
                    wordLength += charInfo.XAdvance;

                    if (c == ' ')
                    {
                        if (wrapWidth != -1) // word wrapping is on
                        {
                            if (wordLength + lineLength > wrapWidth)
                            {
                                lines.Add(line); // old line is archived

                                line = word; // word becomes the new line (start of new line)
                                lineLength = wordLength; // carry over the value
                            }
                            else
                            {
                                line.AddRange(word);
                                lineLength += wordLength;
                            }

                            word = new List<GlyphInfo>();  // reset for next word
                            wordLength = 0;
                        }
                        else
                        {
                            // no word wrapping, just add to list
                            line.AddRange(word);
                            lineLength += wordLength;

                            word = new List<GlyphInfo>();  // reset for next word
                            wordLength = 0;
                        }
                    }
                }

                work.Remove(0, 1);
            }

            if (wrapWidth != -1) // word wrapping is on
            {
                if (wordLength + lineLength > wrapWidth)
                {
                    lines.Add(line); // old line is archived

                    line = word; // word becomes the new line (start of new line)
                    lineLength = wordLength; // carry over the value
                }
                else
                {
                    line.AddRange(word);
                    lineLength += wordLength;
                }

                word = new List<GlyphInfo>();  // reset for next word
                wordLength = 0;
            }
            else
            {
                line.AddRange(word);
                lineLength += wordLength;
            }

            lines.Add(line);

            return new TextSource(this, lines);
        }

        protected static Boolean ControlCheck(String work, out String code)
        {
            code = String.Empty;

            if (ColorCodeRegex.IsMatch(work))
            {
                code = work.Substring(0, 11);
                return true;
            }
            else if (ShortColorCodeRegex.IsMatch(work))
            {
                code = work.Substring(0, 9);
                return true;
            }
            else if (ColorResetRegex.IsMatch(work))
            {
                code = "{#x}";
                return true;
            }

            return false;
        }

        public static BitmapFont DebugFont
        {
            get
            {
                return TSContentManager.Global.Load<BitmapFont>(Configuration.Instance.GetString("debug.font", "Fonts/DebugFont.font-xml"));
            }
        }






    }

    public class BitmapFontReader : BaseContentTypeReader<BitmapFont>
    {
        protected override object Read(ContentReader input, object existingInstance)
        {
            String face = input.ReadString();
            Int32 lineHeight = input.ReadInt32();
            Int32 baseline = input.ReadInt32();

            Int32 pageCount = input.ReadInt32();
            Dictionary<Int32, TextureSource> pages = new Dictionary<int, TextureSource>(pageCount);
            for (Int32 i = 0; i < pageCount; ++i)
            {
                pages.Add(input.ReadInt32(), input.ReadObject<TextureSource>());
            }

            Int32 charCount = input.ReadInt32();
            List<CharacterInfo> chars = new List<CharacterInfo>(charCount);
            for (Int32 i = 0; i < charCount; ++i)
            {
                chars.Add(new CharacterInfo(input.ReadInt32(), new Point(input.ReadInt32(), input.ReadInt32()),
                    new Point(input.ReadInt32(), input.ReadInt32()),
                    new Point(input.ReadInt32(), input.ReadInt32()), input.ReadInt32(), pages[input.ReadInt32()]));
            }

            Int32 kernCount = input.ReadInt32();
            List<KerningInfo> kerns = new List<KerningInfo>(kernCount);
            for (Int32 i = 0; i < kernCount; ++i)
            {
                kerns.Add(new KerningInfo(input.ReadInt32(), input.ReadInt32(), input.ReadInt32()));
            }

            return new BitmapFont(face, lineHeight, baseline, pages, chars, kerns);
        }
    }
}
