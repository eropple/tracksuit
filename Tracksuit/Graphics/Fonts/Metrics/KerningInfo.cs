﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tracksuit.Graphics.Fonts.Metrics
{
    public class KerningInfo
    {
        public readonly Int32 First;
        public readonly Int32 Second;
        public readonly Int32 Amount;

        public KerningInfo(Int32 first, Int32 second, Int32 amount)
        {
            First = first;
            Second = second;
            Amount = amount;
        }
    }
}
