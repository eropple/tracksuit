﻿using Tracksuit.Graphics.Sources;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tracksuit.Graphics.Fonts.Metrics
{
    public class CharacterInfo
    {
        public readonly Int32 ID;
        public readonly Point Offset;
        public readonly Int32 XAdvance;
        public readonly TextureRegionSource Region;

        public CharacterInfo(Int32 id, Point position, Point size, Point offset, Int32 xAdvance, TextureSource page)
        {
            ID = id;
            Offset = offset;
            XAdvance = xAdvance;
            Region = page.CreateRegionSource(new Rectangle(position.X, position.Y, size.X, size.Y));
        }
    }
}
