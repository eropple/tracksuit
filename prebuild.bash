#! /usr/bin/env bash

pushd $(dirname $0) > /dev/null
ROOT=`pwd`


# ./project_init.bash

if [[ `/bin/uname` == *CYGWIN* ]]
then
    BUILDPATH="/cygdrive/c/Program Files (x86)/MSBuild/12.0/Bin/MSBuild.exe"
    TARGET=$ROOT/lib/Windows
fi

rm -rf "$TARGET"
mkdir -p "$TARGET/Debug" "$TARGET/Release"

pushd Dependencies > /dev/null

pushd Jint > /dev/null
    "$BUILDPATH" /t:Jint /p:Configuration=Release '/p:Platform=Any CPU'
    "$BUILDPATH" /t:Jint /p:Configuration=Debug '/p:Platform=Any CPU'
    cp Jint/bin/Release/* $TARGET/Release
    cp Jint/bin/Debug/* $TARGET/Debug
popd > /dev/null

pushd Newtonsoft.Json/Src > /dev/null
    "$BUILDPATH" Newtonsoft.Json.sln /p:Configuration=Release '/p:Platform=Any CPU'
    "$BUILDPATH" Newtonsoft.Json.sln /p:Configuration=Debug '/p:Platform=Any CPU'
    cp Newtonsoft.Json/bin/Release/Net45/* $TARGET/Release
    cp Newtonsoft.Json/bin/Debug/Net45/* $TARGET/Debug
popd > /dev/null

pushd MonoGame > /dev/null
    "$BUILDPATH" MonoGame.Framework.Windows.sln /p:Configuration=Release '/p:Platform=Any CPU'
    "$BUILDPATH" MonoGame.Framework.Windows.sln /p:Configuration=Debug   '/p:Platform=Any CPU'
    cp MonoGame.Framework/bin/Windows/AnyCPU/Release/* $TARGET/Release
    cp MonoGame.Framework/bin/Windows/AnyCPU/Debug/* $TARGET/Debug
popd > /dev/null

pushd Farseer > /dev/null
    pushd SourceFiles > /dev/null
        if [[ ! -f "Farseer Physics MonoGame.csproj.bak" ]]
            then
            cp "Farseer Physics MonoGame.csproj{,.bak}"
        fi
        cp "$TARGET/Release/MonoGame.Framework.dll" .
        sed -i "s,<HintPath>.*,<HintPath>MonoGame.Framework.dll</HintPath>,g" 'Farseer Physics MonoGame.csproj'
    popd > /dev/null
    "$BUILDPATH" /p:Configuration=Release /p:Platform=x86
    "$BUILDPATH" /p:Configuration=Debug   /p:Platform=x86
    cp SourceFiles/bin/WindowsGL/Release/* "$TARGET/Release"
    cp SourceFiles/bin/WindowsGL/Debug/* "$TARGET/Debug"
popd > /dev/null


cp $ROOT/Dependencies/Binaries/* $TARGET/Debug
cp $ROOT/Dependencies/Binaries/* $TARGET/Release

cp $ROOT/Dependencies/Binaries/Windows/* $TARGET/Debug
cp $ROOT/Dependencies/Binaries/Windows/* $TARGET/Release

popd > /dev/null # Dependencies
popd > /dev/null # system root
