﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Tracksuit.Common
{
    public static class XMLUtils
    {
        public static String GetValueFromElement(XElement elem, String defaultValue, Boolean multiline = false)
        {
            String value = defaultValue;
            if (elem != null) value = elem.Value;

            return value == null ? value : String.Join(multiline ? "\n" : " ", value.Replace("\r\n", "\n").Split('\n').Select(t => t.Trim()));
        }

        public static String GetValueFromAttribute(XElement elem, String attributeName, String defaultValue)
        {
            var attr = elem.Attribute(attributeName);
            return attr != null ? attr.Value : defaultValue;
        }
    }
}
