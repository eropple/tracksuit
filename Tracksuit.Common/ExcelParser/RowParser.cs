﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracksuit.Pipeline.ExcelParser
{
    public abstract class RowParser<TOutputType>
        where TOutputType : class
    {
        public readonly Boolean SkipHeaderRow;
        public readonly Func<IList<String>, TOutputType> TransformFunction;

        protected RowParser(bool skipHeaderRow, Func<IList<string>, TOutputType> transformFunction)
        {
            SkipHeaderRow = skipHeaderRow;
            TransformFunction = transformFunction;
        }

        public abstract List<TOutputType> Parse(String path, String worksheetName = null);
    }
}
