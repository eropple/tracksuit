﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Tracksuit.Pipeline.ExcelParser
{
    public class Excel2004XmlRowParser<TOutputType> : RowParser<TOutputType>
        where TOutputType : class
    {
        public Excel2004XmlRowParser(Boolean skipHeaderRow, Func<IList<String>, TOutputType> transformFunction)
            : base (skipHeaderRow, transformFunction)
        {
        }


        public override List<TOutputType> Parse(String path, String worksheetName = null)
        {
            XDocument xml = XDocument.Load(path);
            return Parse(xml, worksheetName);
        }

        private List<TOutputType> Parse(XDocument xml, String worksheetName = null)
        {
//            Debugger.Launch();

            Regex r = new Regex("ss:([A-Za-z0-9]+)");
            String doc = r.Replace(xml.ToString(), "$1");

            XElement root = XDocument.Parse(doc).Root;

            List<TOutputType> output = new List<TOutputType>();

            if (worksheetName != null)
                throw new NotImplementedException("specific worksheets not yet implemented");

            foreach (XElement worksheet in root.Elements())
            {
                if (worksheet.Name.LocalName != "Worksheet") continue;

                foreach (XElement table in worksheet.Elements())
                {
                    if (table.Name.LocalName != "Table") continue;

                    var ccount = table.Attribute("ExpandedColumnCount");
                    Int32 columnCount = ccount != null
                        ? Int32.Parse(ccount.Value)
                        : table.Elements().Max(elem => elem.Elements().Count());
                    Int32 rowIndex = 1;
                    foreach (XElement row in table.Elements())
                    {
                        if (row.Name.LocalName != "Row") continue;

                        if (row.Attribute("Index") != null)
                            rowIndex = Int32.Parse(row.Attribute("Index").Value);

                        if (SkipHeaderRow && rowIndex == 1) { ++rowIndex; continue; }

                        String[] columns = new String[columnCount];

                        Int32 columnIndex = 1;
                        foreach (XElement col in row.Elements())
                        {
                            if (col.Name.LocalName != "Cell") continue;
                            if (col.Attribute("Index") != null)
                                columnIndex = Int32.Parse(col.Attribute("Index").Value);

                            columns[columnIndex - 1] = col.Value;
                            ++columnIndex;
                        }

                        TOutputType result = TransformFunction(columns);
                        if (result != null)
                        {
                            output.Add(result);
                        }

                        ++rowIndex;
                    }
                }
            }

            return output;
        }
    }
}
