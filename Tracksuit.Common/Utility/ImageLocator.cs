﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tracksuit.Common.Utility
{
    public struct ImageLocator
    {
        public readonly String ImagePath;
        public readonly ImmutableRectangle? Rectangle;

        public ImageLocator(string imagePath, ImmutableRectangle? rectangle) : this()
        {
            ImagePath = imagePath;
            Rectangle = rectangle;
        }

        public bool Equals(ImageLocator other)
        {
            return string.Equals(ImagePath, other.ImagePath) && Rectangle.Equals(other.Rectangle);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ImageLocator && Equals((ImageLocator) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((ImagePath != null ? ImagePath.GetHashCode() : 0)*397) ^ Rectangle.GetHashCode();
            }
        }
    }
}
