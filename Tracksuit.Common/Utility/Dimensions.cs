﻿namespace Tracksuit.Common.Utility
{
    public struct Dimensions<T>
        where T : struct
    {
        public readonly T Width;
        public readonly T Height;

        public Dimensions(T width, T height)
        {
            Width = width;
            Height = height;
        }
    }
}
