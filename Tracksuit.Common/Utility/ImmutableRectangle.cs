﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Tracksuit.Common.Utility
{
    public struct ImmutableRectangle : IEquatable<ImmutableRectangle>, IEquatable<Rectangle>
    {
        public readonly Int32 X;
        public readonly Int32 Y;
        public readonly Int32 Width;
        public readonly Int32 Height;

        public ImmutableRectangle(int x, int y, int width, int height) : this()
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public int Left { get { return X; } }
        public int Right { get { return X + Width; } }
        public int Top { get { return Y; } }
        public int Bottom { get { return Y + Height; } }


        public bool Equals(ImmutableRectangle other)
        {
            return X == other.X && Y == other.Y && Width == other.Width && Height == other.Height;
        }

        public bool Equals(Rectangle other)
        {
            return X == other.X && Y == other.Y && Width == other.Width && Height == other.Height;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ImmutableRectangle && Equals((ImmutableRectangle) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = X;
                hashCode = (hashCode*397) ^ Y;
                hashCode = (hashCode*397) ^ Width;
                hashCode = (hashCode*397) ^ Height;
                return hashCode;
            }
        }


        public static explicit operator Rectangle(ImmutableRectangle r)
        {
            return new Rectangle(r.X, r.Y, r.Width, r.Height);
        }
    }
}
