﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Tracksuit.Common.Collections
{
    public sealed class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        private readonly IDictionary<TKey, TValue> _underlying;

        public ReadOnlyDictionary(IDictionary<TKey, TValue> underlying)
        {
            _underlying = underlying;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _underlying.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw new InvalidOperationException("Cannot be invoked on ReadOnlyDictionary.");
        }

        public void Clear()
        {
            throw new InvalidOperationException("Cannot be invoked on ReadOnlyDictionary.");
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _underlying.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _underlying.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new InvalidOperationException("Cannot be invoked on ReadOnlyDictionary.");
        }

        public int Count
        {
            get { return _underlying.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool ContainsKey(TKey key)
        {
            return _underlying.ContainsKey(key);
        }

        public void Add(TKey key, TValue value)
        {
            throw new InvalidOperationException("Cannot be invoked on ReadOnlyDictionary.");
        }

        public bool Remove(TKey key)
        {
            throw new InvalidOperationException("Cannot be invoked on ReadOnlyDictionary.");
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _underlying.TryGetValue(key, out value);
        }

        public TValue this[TKey key]
        {
            get { return _underlying[key]; }
            set { throw new InvalidOperationException("Cannot be invoked on ReadOnlyDictionary."); }
        }

        public ICollection<TKey> Keys
        {
            get { return _underlying.Keys; }
        }

        public ICollection<TValue> Values
        {
            get { return _underlying.Values; }
        }
    }
}
