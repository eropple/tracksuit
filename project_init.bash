#! /usr/bin/env bash

git submodule update --init --recursive

pushd Dependencies/MonoGame
git submodule update --init --recursive
./Protobuild.exe
popd

pushd Dependencies
svn checkout https://farseerphysics.svn.codeplex.com/svn Farseer
popd
